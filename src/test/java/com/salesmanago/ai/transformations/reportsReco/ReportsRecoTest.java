package com.salesmanago.ai.transformations.reportsReco;

import com.salesmanago.ai.basicData.Visits;
import com.salesmanago.ai.basicData.helpers.productRecoVisitedTogether.ProductMap;
import com.salesmanago.ai.basicData.helpers.productRecoVisitedTogether.ProductRecoMetadata;
import com.salesmanago.ai.transformations.ReportsReco;
import com.salesmanago.ai.utils.ScoringFunctions;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.catalyst.expressions.GenericRowWithSchema;
import org.apache.spark.sql.types.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import scala.collection.mutable.WrappedArray;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static com.salesmanago.ai.utils.Constants.SCORE_COLUMN;
import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.junit.jupiter.api.Assertions.*;


class ReportsRecoTest {

    @ParameterizedTest
    @MethodSource("provideGroupedByContactData")
    void reduceToRecoStructureVisitedTogetherTest(List<Integer> pids, List<Row> productTypeDate) {
        final List<Row> originalRows = originalTranformationInMap(pids, productTypeDate);
        final List<Row> transformedRows = ReportsReco.reduceToRecoStructure(productTypeDate, "vt");

        assertEquals(originalRows.size(), transformedRows.size());
        IntStream
                .range(0, Math.min(originalRows.size(), transformedRows.size()))
                .mapToObj(i -> Pair.of(originalRows.get(i), transformedRows.get(i)))
                .forEach(rowPair -> {
                    assertEquals(rowPair.getLeft().getInt(0), rowPair.getRight().getInt(0));
                    IntStream.range(1, 4)
                            .forEach(i -> {
                                final Object[] o1 = (Object[]) rowPair.getLeft().get(i);
                                final List<String> expectedList = Arrays.stream(o1).map(String::valueOf).collect(Collectors.toList());
                                final Object[] o2 = (Object[]) rowPair.getRight().get(i);
                                final List<String> actualList = Arrays.stream(o2).map(String::valueOf).collect(Collectors.toList());
                                assertIterableEquals(expectedList, actualList);
                            });
                });
    }

    private static Stream<Arguments> provideGroupedByContactData() throws Exception {
        return Stream.of(Arrays.asList(1, 4, 5, 100, 12310),
                Arrays.asList(2, 3, 6, 101, 13310),
                Arrays.asList(1, 3, 101, 104, 17310),
                Arrays.asList(5, 1, 12, 100, 13310),
                Arrays.asList(3, 1, 2, 10, 13310),
                Arrays.asList(5, 4, 10, 100, 13310),
                Arrays.asList(5, 10, 12, 10, 13310),
                Arrays.asList(5, 100, 2, 10, 13310),
                Arrays.asList(5, 4, 2, 100, 13310),
                Arrays.asList(5, 3, 12, 10, 13310),
                Arrays.asList(4, 12310))
                .map(l -> Arguments.of(l, genereateListOfRowExtended(l)));
    }

    private static List<Row> genereateListOfRowExtended(List<Integer> list) {
        return list.stream()
                .map(i -> RowFactory
                        .create(i,
                                "2019-07-10 11:" + (10 + new Random().nextInt(50)) + ":35",
                                "view",
                                1002,
                                123))
                .collect(Collectors.toList());
    }


    /* INFO:
            This is only helper very OLD function - raw version that does what it supposed to be doing
            to check if refactoring does not breaks fundamental transfrormation
    */
    private static List<Row> originalTranformationInMap(List<Integer> pids, List<Row> productTypeDate) {
        return pids.stream()
                .map(pid -> {
                    Row currentRow = productTypeDate.stream()
                            .filter(r -> pid.equals(r.getInt(0)))
                            .findFirst().orElse(Row.empty());
                    ProductMap map = new ProductMap(Integer.valueOf(pid), currentRow.getInt(3), currentRow.getInt(4));
                    productTypeDate.stream()
                            .filter(r -> !pid.equals(r.getInt(0)))
                            .forEach(r -> map.addToMap(r.getInt(0),
                                    ScoringFunctions.scoreVisitedTogether(Pair.of(currentRow, r)),
                                    r.getInt(3),
                                    r.getInt(4)));
                    final List<Integer> recoPids = new ArrayList<>(map.getMap().keySet());
                    final List<Integer> recoScores = map.getMap().values().stream().map(ProductRecoMetadata::getScore).collect(Collectors.toList());
                    final List<Integer> recoShops = map.getMap().values().stream().map(ProductRecoMetadata::getShopId).collect(Collectors.toList());
                    return RowFactory.create(map.getProductId(),
                            recoPids.isEmpty() ? null : recoPids.toArray(),
                            recoScores.isEmpty() ? null : recoScores.toArray(),
                            recoShops.isEmpty() ? null : recoShops.toArray());
                })
                .filter(r -> !r.isNullAt(1))
                .collect(Collectors.toList());
    }

    @ParameterizedTest
    @MethodSource("provideScoringDataGroupedByProduct")
    void reduceToRecoProductVisitedTogetherTest(Row row) throws Exception {
        final Row originalRow = reduceToRecoProductVisitedTogether(row);
        final Row actualRow = ReportsReco.reduceToRecoSugestedProduct(row);

        assertEquals(originalRow.getInt(0), actualRow.getInt(0));
        assertEquals(originalRow.getInt(1), actualRow.getInt(1));
        IntStream.range(2, 5)
                .forEach(i -> {
                    final Object[] o1 = (Object[]) originalRow.get(i);
                    final List<String> expectedList = Arrays.stream(o1).map(String::valueOf).collect(Collectors.toList());
                    final Object[] o2 = (Object[]) actualRow.get(i + 1);
                    final List<String> actualList = Arrays.stream(o2).map(String::valueOf).collect(Collectors.toList());
                    assertIterableEquals(expectedList, actualList);
                });
    }

    private static Stream<Arguments> provideScoringDataGroupedByProduct() {
        Random random = new Random();
        int arraysSize = random.nextInt(15);
        return IntStream.range(1, 12)
                .mapToObj(i -> {
                    final List<Integer> pids = random.ints(arraysSize, 1, 8)
                            .mapToObj(Integer::valueOf)
                            .collect(Collectors.toList());
                    final List<Integer> scores = random.ints(arraysSize, 1, 1000)
                            .mapToObj(Integer::valueOf)
                            .collect(Collectors.toList());
                    final List<Integer> shops = random.ints(arraysSize)
                            .mapToObj(i1 -> 1015)
                            .collect(Collectors.toList());
                    return Arguments.of(
                            new GenericRowWithSchema(new Object[]{
                                    i,
                                    1014,
                                    500,
                                    WrappedArray.make(IntStream.range(0, arraysSize)
                                            .mapToObj(index -> new GenericRowWithSchema(new Object[]{
                                                    pids.get(index),
                                                    scores.get(index),
                                                    shops.get(index),
                                                    500},
                                                    new StructType()
                                                            .add("pid", DataTypes.IntegerType)
                                                            .add(SCORE_COLUMN, DataTypes.IntegerType)
                                                            .add("shop", DataTypes.IntegerType)
                                                            .add("category", DataTypes.IntegerType))).toArray())},
                                    new StructType()
                                            .add("pid", DataTypes.IntegerType)
                                            .add("schopId", DataTypes.IntegerType)
                                            .add("categoryIntId", DataTypes.IntegerType)
                                            .add("zipped", new StructType(new StructField[]{
                                                    new StructField("pid", DataTypes.IntegerType, true, Metadata.empty()),
                                                    new StructField(SCORE_COLUMN, DataTypes.IntegerType, true, Metadata.empty()),
                                                    new StructField("shop", DataTypes.IntegerType, true, Metadata.empty()),
                                                    new StructField("category", DataTypes.IntegerType, true, Metadata.empty())})
                                            )));
                });
    }


    /* INFO:
             This is only helper very OLD function - raw version that does what it supposed to be doing
             to check if refactoring does not breaks fundamental transfrormation
    */
    private static Row reduceToRecoProductVisitedTogether(Row row) {
        final List<Row> list = row.getList(3);
        final HashMap<Map<Integer, Integer>, Integer> reducedRecomendations = list.stream()
                .reduce(new HashMap<Map<Integer, Integer>, Integer>(),
                        (acc, r) -> {
                            acc.compute(new HashMap<Integer, Integer>() {{
                                put(r.getInt(0), r.getInt(2));
                            }}, (key, val) -> Objects.nonNull(val)
                                    ? val + r.getInt(1)
                                    : r.getInt(1));
                            return acc;

                        }, (a, b) -> {
                            a.putAll(b);
                            return a;
                        })
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                        LinkedHashMap::new));
        return RowFactory.create(row.getInt(0),
                row.getInt(1),
                reducedRecomendations.keySet().stream()
                        .map(s -> s.keySet().isEmpty()
                                ? null
                                : s.keySet().iterator().next()).toArray(),
                reducedRecomendations.values().toArray(),
                reducedRecomendations.keySet().stream()
                        .map(s -> s.values().isEmpty()
                                ? null
                                : s.values().iterator().next()).toArray());
    }

    @ParameterizedTest
    @MethodSource("provideDataToFilterScrappers")
    void webScrapperFilterTest(List<Row> productTypeDate) {
        assertEquals(productTypeDate.size() > 1000, Visits.isUserWebScrapper(productTypeDate
                        .stream()
                        .map(row -> row.getString(1))
                        .collect(toList()),
                1));
    }

    private static Stream<Arguments> provideDataToFilterScrappers() throws Exception {
        return Stream.of(IntStream.range(1, 100000).mapToObj(Integer::valueOf).collect(Collectors.toList()),
                IntStream.range(1, 980).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 567).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 200).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 1100).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 5450).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 2200).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 1001).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 999).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 100).mapToObj(Integer::valueOf).collect(toList()))
                .map(l -> Arguments.of(genereateListOfRow(l)));
    }

    private static List<Row> genereateListOfRow(List<Integer> list) {
        return list.stream()
                .map(i -> RowFactory
                        .create(i,
                                "2019-07-10 11:" + (10 + new Random().nextInt(50)) + ":35",
                                123,
                                1002))
                .collect(Collectors.toList());
    }
}
