package com.salesmanago.ai.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;

class UriParserTest {

    @ParameterizedTest
    @CsvSource({
            ",0",
            "  ,0",
            "utm_content=bodywww&utm_source=automails&utm_medium=email&utm_campaign=dostepny,4",
            "utm_content=b&utm_source=&utm_medium=&utm_campaign=,4",
            "page%3D3,1",
            "page%3D%26%3D%26,1",
            "page%3D3%26param%3Dcos%26param1%3D%26param2%3D,4"})
    void splitQueryTestWithUncodedParameters(String url_params, Integer paramsCount) {
        final Map map = UriParser.splitQuery(url_params);
        assertEquals(paramsCount, map.size());
    }
}