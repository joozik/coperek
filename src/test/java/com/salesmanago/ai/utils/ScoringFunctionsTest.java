package com.salesmanago.ai.utils;

import org.apache.calcite.util.Pair;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;
import static org.junit.jupiter.api.Assertions.*;

class ScoringFunctionsTest {

    @ParameterizedTest
    @MethodSource("provideData")
    public void test(List<Row> list) {
        final List<Row> result = ScoringFunctions.reduceProducShopScoreToListOfRows(list, "contactId", 10);
        final List<Row> expected =  moreImperativeHelper(list, "contactId", 10);

        assertEquals(expected.size(), result.size());
        assertTrue(result.size() <= 10);

        final Iterator<Row> resultIterator = result.iterator();
        final Iterator<Row> expectedIterator = expected.iterator();

        while (resultIterator.hasNext()) {
            Row r = resultIterator.next();
            Row e = expectedIterator.next();
            IntStream.range(1, 3)
                    .forEach(i -> assertEquals(e.getInt(i), r.getInt(i)));
        }


    }

    /* INFO:
                This is only helper very OLD function - raw version that does what it supposed to be doing
                to check if refactoring does not breaks fundamental transfrormation
    */
    private List<Row> moreImperativeHelper(List<Row> list, String contactId, Integer limit) {
        HashMap<Pair<Integer, Integer>, Integer> map = new HashMap<>();
        list.forEach(l ->
                map.compute(Pair.of(l.getInt(0), l.getInt(1)),(key, val)
                        -> (val == null)
                        ? l.getInt(2)
                        : val + l.getInt(2)));
        final List<Row> collect = map.entrySet().stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .limit(limit)
                .map(entry -> RowFactory.create(contactId, entry.getKey().left, entry.getValue(), entry.getKey().right))
                .collect(toList());
        return collect;
    }


    private static Stream<Arguments> provideData() {
        return Stream.of(IntStream.range(1, 11).mapToObj(Integer::valueOf).collect(Collectors.toList()),
                IntStream.range(1, 9).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 5).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 32).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 211).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 54).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 22).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 114).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 19).mapToObj(Integer::valueOf).collect(toList()),
                IntStream.range(1, 87).mapToObj(Integer::valueOf).collect(toList()))
                .map(l -> Arguments.of(genereateListOfRow(l)));
    }

    private static List<Row> genereateListOfRow(List<Integer> list) {
        return list.stream()
                .map(i -> RowFactory
                        .create(i % 10 + 1,
                                i % 2 == 0 ? 1002 : 1001,
                                i))
                .collect(Collectors.toList());
    }
}