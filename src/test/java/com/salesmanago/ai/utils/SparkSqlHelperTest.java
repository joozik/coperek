package com.salesmanago.ai.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Stream;

import static com.salesmanago.ai.utils.Constants.*;
import static com.salesmanago.ai.utils.Constants.PRODUCT_EXT_ID_COLUMN;
import static org.junit.jupiter.api.Assertions.*;

class SparkSqlHelperTest {

    @ParameterizedTest
    @MethodSource("provideData")
    void replaceParamsTest(String template, Map<String, String> params, String expected) {
        assertEquals(expected, SparkSqlHelper.replaceParams(template, params));
    }

    private static Stream<Arguments> provideData() {
        return Arrays.asList(
                Arguments.of("SELECT t.%(transactionDate), t.%(contactId), '%(purchaseVisitType)' AS %(visitType), "
                                + "CAST(p.%(productId) AS int) %(productId), %(purchaseScoreConst) AS %(score) FROM %(transactionTable) AS t "
                                + "INNER JOIN %(productTable) AS p ON t.%(productExtId) = p.%(productExtId)",
                        new HashMap<String, String>() {
                            {
                                put("transactionDate", TRANSACTION_DATE_COLUMN);
                                put("contactId", CONTACT_ID_COLUMN);
                                put("purchaseVisitType", PURCHASE_VISIT_TYPE);
                                put("visitType", VISIT_TYPE_COLUMN);
                                put("productId", PRODUCT_ID_COLUMN);
                                put("purchaseScoreConst", "50");
                                put("score", SCORE_COLUMN);
                                put("transactionTable", TRANSACTION_TABLE);
                                put("productTable", PRODUCT_TABLE);
                                put("productExtId", PRODUCT_EXT_ID_COLUMN);
                            }
                        },
                        "SELECT t." + TRANSACTION_DATE_COLUMN + ", t." + CONTACT_ID_COLUMN + ", '"
                                + PURCHASE_VISIT_TYPE + "' AS " + VISIT_TYPE_COLUMN + ","
                                + " CAST(p." + PRODUCT_ID_COLUMN + " AS int) " + PRODUCT_ID_COLUMN + ", 50 AS " + SCORE_COLUMN
                                + " FROM " + TRANSACTION_TABLE + " AS t"
                                + " INNER JOIN " + PRODUCT_TABLE + " AS p"
                                + " ON t." + PRODUCT_EXT_ID_COLUMN + " = p." + PRODUCT_EXT_ID_COLUMN),

                Arguments.of("SELECT p.%(productId), p.%(productExtId), p.%(category), p.%(subCategory), "
                                + "p.%(categoryIntId), p.%(shopId), p.%(url), '' AS %(urlParameters), "
                                + "t.%(contactId), '' AS %(uuIdIntId), t.%(transactionDate), "
                                + "'%(purchaseVisitType)' AS %(visitType) "
                                + "FROM %(transactionTable) AS t "
                                + "INNER JOIN %(productTable) AS p "
                                + "ON t.%(productExtId) = p.%(productExtId)",
                        new HashMap<String, String>() {
                            {
                                put("productId", PRODUCT_ID_COLUMN);
                                put("productExtId", PRODUCT_EXT_ID_COLUMN);
                                put("category", CATEGORY_COLUMN);
                                put("subCategory", SUBCATEGORY_COLUMN);
                                put("categoryIntId", CATEGORY_INT_ID_COLUMN);
                                put("shopId", SHOP_ID_COLUMN);
                                put("url", URL_COLUMN);
                                put("urlParameters", URL_PARAMETERS_COLUMN);
                                put("contactId", CONTACT_ID_COLUMN);
                                put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                                put("transactionDate", TRANSACTION_DATE_COLUMN);
                                put("purchaseVisitType", PURCHASE_VISIT_TYPE);
                                put("visitType", VISIT_TYPE_COLUMN);
                                put("purchaseScoreConst", "50");
                                put("score", SCORE_COLUMN);
                                put("transactionTable", TRANSACTION_TABLE);
                                put("productTable", PRODUCT_TABLE);
                            }
                        },
                        "SELECT p." + PRODUCT_ID_COLUMN + ", p." + PRODUCT_EXT_ID_COLUMN + ", p."
                                + CATEGORY_COLUMN + ", p.subCategory, p." + CATEGORY_INT_ID_COLUMN + ","
                                + " p." + SHOP_ID_COLUMN + ", p." + URL_COLUMN + ", '' AS " + URL_PARAMETERS_COLUMN
                                + ", t." + CONTACT_ID_COLUMN + ", '' AS " + UU_ID_INT_ID_COLUMN + ", t." + TRANSACTION_DATE_COLUMN + ", '"
                                + PURCHASE_VISIT_TYPE + "' AS " + VISIT_TYPE_COLUMN
                                + " FROM " + TRANSACTION_TABLE + " AS t"
                                + " INNER JOIN " + PRODUCT_TABLE + " AS p"
                                + " ON t." + PRODUCT_EXT_ID_COLUMN + " = p." + PRODUCT_EXT_ID_COLUMN),
                Arguments.of("SELECT * FROM %(visitTable) AS v "
                                + "LEFT ANTI JOIN %(anonymousBotTable) AS a ON v.%(uuIdIntId) = a.%(uuIdIntId) "
                                + "LEFT ANTI JOIN %(contactBotTable) AS c ON v.%(contactId) = c.%(contactId)",
                        new HashMap<String, String>() {
                            {
                                put("visitTable", VISIT_TABLE);
                                put("anonymousBotTable", ANONYMOUS_BOT_CANDIDATES_TABLE);
                                put("contactBotTable", CONTACT_BOT_CANDIDATES_TABLE);
                                put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                                put("contactId", CONTACT_ID_COLUMN);
                            }
                        },
                        "SELECT * FROM " + VISIT_TABLE + " AS v"
                                + " LEFT ANTI JOIN " + ANONYMOUS_BOT_CANDIDATES_TABLE + " AS a"
                                + " ON v." + UU_ID_INT_ID_COLUMN + " = a." + UU_ID_INT_ID_COLUMN
                                + " LEFT ANTI JOIN " + CONTACT_BOT_CANDIDATES_TABLE + " AS c"
                                + " ON v." + CONTACT_ID_COLUMN + " = c." + CONTACT_ID_COLUMN),
                Arguments.of("SELECT %(contactInfo), "
                                + "ARRAYS_ZIP(COLLECT_LIST(%(productId)), "
                                + "COLLECT_LIST(%(date)), "
                                + "COLLECT_LIST(%(visitType)), "
                                + "COLLECT_LIST(%(shopId)), "
                                + "COLLECT_LIST(%(category))) AS combinedData "
                                + "FROM %(productVisitTable) "
                                + "%(whereClause) "
                                + "GROUP BY %(contactInfo)",
                        new HashMap<String, String>() {
                            {
                                put("contactInfo", UU_ID_INT_ID_COLUMN);
                                put("productId", PRODUCT_ID_COLUMN);
                                put("date", DATE_COLUMN);
                                put("visitType", VISIT_TYPE_COLUMN);
                                put("shopId", SHOP_ID_COLUMN);
                                put("category", CATEGORY_COLUMN);
                                put("productVisitTable", PRODUCT_VISIT_TABLE);
                                put("whereClause", SparkSqlHelper.recoGroupingWhereClause(UU_ID_INT_ID_COLUMN));
                            }
                        },
                        "SELECT " + UU_ID_INT_ID_COLUMN + ","
                                + " ARRAYS_ZIP(COLLECT_LIST(" + PRODUCT_ID_COLUMN + "),"
                                + " COLLECT_LIST(" + DATE_COLUMN + "),"
                                + " COLLECT_LIST(" + VISIT_TYPE_COLUMN + "),"
                                + " COLLECT_LIST(" + SHOP_ID_COLUMN + "),"
                                + " COLLECT_LIST(" + CATEGORY_COLUMN + ")) AS combinedData"
                                + " FROM " + PRODUCT_VISIT_TABLE
                                + " WHERE " + CONTACT_ID_COLUMN + " IS NULL"
                                + " GROUP BY " + UU_ID_INT_ID_COLUMN),
                Arguments.of(
                        "SELECT %(contactId), %(uuIdIntId), %(date) FROM %(visitTable) %(whereClause)",
                        new HashMap<String, String>() {
                            {
                                put("contactId", CONTACT_ID_COLUMN);
                                put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                                put("date", DATE_COLUMN);
                                put("visitTable", VISIT_TABLE);
                                put("whereClause", SparkSqlHelper.recoGroupingWhereClause(UU_ID_INT_ID_COLUMN));
                            }
                        },
                        "SELECT " + CONTACT_ID_COLUMN + ", " + UU_ID_INT_ID_COLUMN + ", " + DATE_COLUMN
                                + " FROM " + VISIT_TABLE
                                + " " + SparkSqlHelper.recoGroupingWhereClause(UU_ID_INT_ID_COLUMN)),
                Arguments.of("SELECT CAST(p.%(productId) AS int) %(productId), p.%(category), "
                        + "p.%(subCategory), CAST(p.%(shopId) AS int) %(shopId), v.%(contactId), "
                        + "v.%(uuIdIntId), v.%(date), '%(visitTypeConst)' AS %(visitType) "
                        + "FROM %(visitFilteredTable) AS v "
                        + "INNER JOIN %(productTable) AS p ON v.%(url) = p.%(url)",
                        new HashMap<String, String>(){
                            {
                                put("productId", PRODUCT_ID_COLUMN);
                                put("category", CATEGORY_COLUMN);
                                put("subCategory", SUBCATEGORY_COLUMN);
                                put("shopId", SHOP_ID_COLUMN);
                                put("contactId", CONTACT_ID_COLUMN);
                                put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                                put("date", DATE_COLUMN);
                                put("visitTypeConst", "view");
                                put("visitType", VISIT_TYPE_COLUMN);
                                put("visitFilteredTable", VISIT_FILTERED_TABLE);
                                put("productTable", PRODUCT_TABLE);
                                put("url", URL_COLUMN);
                            }
                        },
                        "SELECT CAST(p." + PRODUCT_ID_COLUMN + " AS int) " + PRODUCT_ID_COLUMN + ","
                                + " p." + CATEGORY_COLUMN + ", p." + SUBCATEGORY_COLUMN + ", CAST(p." + SHOP_ID_COLUMN + " AS int) "
                                + SHOP_ID_COLUMN + ", v." + CONTACT_ID_COLUMN + ", v." + UU_ID_INT_ID_COLUMN + ", v."
                                + DATE_COLUMN + ", 'view' AS " + VISIT_TYPE_COLUMN + " FROM " + VISIT_FILTERED_TABLE
                                + " AS v INNER JOIN " + PRODUCT_TABLE + " AS p ON v." + URL_COLUMN + " = p." + URL_COLUMN),
                Arguments.of("SELECT v.%(date), v.%(contactId), p.%(productId), v.%(urlParameters) "
                        + "FROM %(productTable) AS p "
                        + "INNER JOIN %(visitTable) AS v ON p.%(url) = v.%(url)",
                        new HashMap<String, String>(){
                            {
                                put("date", DATE_COLUMN);
                                put("contactId", CONTACT_ID_COLUMN);
                                put("productId", PRODUCT_ID_COLUMN);
                                put("urlParameters", URL_PARAMETERS_COLUMN);
                                put("productTable", PRODUCT_TABLE);
                                put("visitTable", VISIT_TABLE);
                                put("url", URL_COLUMN);
                            }
                        },
                        "SELECT v." + DATE_COLUMN + ", v." + CONTACT_ID_COLUMN + ", p."
                                + PRODUCT_ID_COLUMN + ", v." + URL_PARAMETERS_COLUMN + " FROM " + PRODUCT_TABLE + " AS p"
                                + " INNER JOIN " + VISIT_TABLE + " AS v ON p." + URL_COLUMN + " = v." + URL_COLUMN
                )
        )
                .stream();
    }

}