package com.salesmanago.ai.transformations;

import com.salesmanago.ai.utils.SparkSqlHelper;
import com.salesmanago.ai.utils.UriParser;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.util.*;

import static com.salesmanago.ai.basicData.BasicStructures.*;
import static com.salesmanago.ai.utils.Constants.*;

public class ProductsExtended {

    public static List<String> buildProductVisitsFromVisits(SparkSession spark) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT CAST(p.%(productId) AS int) %(productId), p.%(uniqueId), "
                        + "p.%(category), p.%(categoryIntId), CAST(p.%(shopId) AS int) %(shopId), v.%(contactId), "
                        + "v.%(uuIdIntId), v.%(date), '%(visitTypeConst)' AS %(visitType), v.%(url), v.%(urlParameters) "
                        + "FROM %(visitFilteredTable) AS v "
                        + "INNER JOIN %(productTable) AS p ON v.%(url) = p.%(url)",
                new HashMap<String, String>() {
                    {
                        put("productId", PRODUCT_ID_COLUMN);
                        put("uniqueId", PRODUCT_UNIQUE_ID_COLUMN);
                        put("category", CATEGORY_COLUMN);
                        put("categoryIntId",CATEGORY_INT_ID_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("contactId", CONTACT_ID_COLUMN);
                        put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                        put("date", DATE_COLUMN);
                        put("visitTypeConst", VIEW_VISIT_TYPE);
                        put("visitType", VISIT_TYPE_COLUMN);
                        put("url", URL_COLUMN);
                        put("urlParameters", URL_PARAMETERS_COLUMN);
                        put("visitFilteredTable", VISIT_FILTERED_TABLE);
                        put("productTable", PRODUCT_TABLE);
                    }
                }))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(PRODUCT_VISIT_TABLE);
        return Collections.singletonList(PRODUCT_VISIT_TABLE);
    }

    public static Dataset<Row> buildExtractedVisits(SparkSession spark) {
        return spark.sql(SparkSqlHelper.replaceParams("SELECT %(date), %(contactId), %(productId), "
                        + "%(urlParameters), %(shopId), %(uniqueId) "
                        + "FROM %(productVisitTable) "
                        + "WHERE %(visitType) = '%(viewVisitType)'",
                new HashMap<String, String>() {
                    {
                        put("date", DATE_COLUMN);
                        put("contactId", CONTACT_ID_COLUMN);
                        put("productId", PRODUCT_ID_COLUMN);
                        put("urlParameters", URL_PARAMETERS_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("uniqueId", PRODUCT_UNIQUE_ID_COLUMN);
                        put("visitType", VISIT_TYPE_COLUMN);
                        put("viewVisitType", VIEW_VISIT_TYPE);
                        put("productVisitTable", PRODUCT_VISIT_TABLE);
                    }
                }))
                .map((MapFunction<Row, Row>) row ->
                                RowFactory.create(row.getString(0),
                                        row.getString(1),
                                        VIEW_VISIT_TYPE,
                                        row.getInt(2),
                                        row.getInt(4),
                                        row.getInt(5),
                                        scoreVisit(row.getString(3))),
                        RowEncoder.apply(EXPORT_VISIT_STRUCT_WITH_SCORE));
    }

    private static int scoreVisit(String urlParameters) {
        if (Objects.nonNull(urlParameters) && urlParameters.contains("utm_source")) {
            final Map<String, List<String>> parametersMap = UriParser.splitQuery(urlParameters);
            if (parametersMap.containsKey("utm_medium")) {
                if (parameterEquals("salesmanago", parametersMap.get("utm_source"))
                        && parameterEquals("rule", parametersMap.get("utm_medium"))) {
                    return 7;
                } else if (parameterEquals("salesmanago", parametersMap.get("utm_source"))
                        && parameterEquals("email", parametersMap.get("utm_medium"))) {
                    return 6;
                } else if (parameterEquals("autoprom", parametersMap.get("utm_source"))
                        && parameterEquals("email", parametersMap.get("utm_medium"))) {
                    return 5;
                } else if (parameterEquals("criteo", parametersMap.get("utm_source"))
                        && parameterEquals("display", parametersMap.get("utm_medium"))) {
                    return 4;
                }
                return 3;
            } else {
                return 2;
            }
        } else {
            return 1;
        }
    }

    private static boolean parameterEquals(String value, List<String> paramEntries) {
        return Objects.nonNull(paramEntries) && paramEntries.contains(value);
    }

}
