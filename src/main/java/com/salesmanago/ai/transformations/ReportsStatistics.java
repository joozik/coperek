package com.salesmanago.ai.transformations;

import com.salesmanago.ai.utils.Constants;
import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;
import org.apache.spark.sql.SaveMode;
import org.apache.spark.sql.SparkSession;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.*;
import java.util.stream.Collectors;

import static com.salesmanago.ai.utils.Constants.*;
import static java.util.stream.Collectors.*;

public class ReportsStatistics {

    public static List<String> statisticRecommendation(SparkSession spark, String inputTableName) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT " +
                        "%(shopId), %(categoryId), FLATTEN(COLLECT_LIST(ARRAYS_ZIP(%(categoryIntIds), " +
                        "%(recoProdShopId), %(recoProdScore)))) AS combinedData " +
                        "FROM %(inputTable) " +
                        "GROUP BY %(shopId), %(categoryId)",
                new HashMap<String, String>() {
                    {
                        put("shopId", SHOP_ID_COLUMN);
                        put("categoryId", CATEGORY_INT_ID_COLUMN);
                        put("categoryIntIds", "categoryIntIds");
                        put("recoProdShopId", "recoProdShopId");
                        put("recoProdScore", "recoProdScore");
                        put("inputTable", inputTableName);
                    }
                }))
                .map((MapFunction<Row, Row>) row -> reduceStatisticRecomendation(row,
                        RECO_SUGGESTED_PRODUCT_LIMIT,
                        Constants.getType(inputTableName)),
                        RowEncoder.apply(new StructType()
                                .add(SHOP_ID_COLUMN, DataTypes.IntegerType)
                                .add(CATEGORY_INT_ID_COLUMN, DataTypes.IntegerType)
                                .add("categoryIntIds", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdScore", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdShopId", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("type", DataTypes.StringType)))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(STATISTIC_RECOMMENDATION_TABLE);
        return Collections.singletonList(STATISTIC_RECOMMENDATION_TABLE);
    }

    private static Row reduceStatisticRecomendation(Row row, Long limit, String type) {
        final List<Row> list = row.getList(2);
        return list.stream().map(r ->
                new AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>(Pair.of(r.getInt(0),
                        r.getInt(1)),
                        r.getInt(2)))
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>::getKey,
                        LinkedHashMap<Pair<Integer, Integer>, Integer>::new,
                        Collectors.summingInt(AbstractMap.SimpleImmutableEntry::getValue)),
                        pairIntegerLinkedHashMap -> {
                            LinkedHashMap<Pair<Integer, Integer>, Integer> map = pairIntegerLinkedHashMap.entrySet().stream()
                                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                                    .limit(limit)
                                    .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                                            Integer::sum,
                                            LinkedHashMap::new));
                            return RowFactory.create(row.getInt(0),
                                    row.getInt(1),
                                    map.keySet().stream().map(Pair::getLeft).collect(toList()).toArray(),
                                    map.values().toArray(),
                                    map.keySet().stream().map(Pair::getRight).collect(toList()).toArray(),
                                    type);
                        }));
    }

    public static List<String> statisticCategoryRecommendation(SparkSession spark, String inputTableName) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT " +
                        "%(shopId), %(categoryId), FLATTEN(COLLECT_LIST(ARRAYS_ZIP(%(recoProducts), " +
                        "%(categoryIntIds), %(recoProdShopId), %(recoProdScore)))) AS combinedData " +
                        "FROM %(inputTable) " +
                        "GROUP BY %(shopId), %(categoryId)",
                new HashMap<String, String>() {
                    {
                        put("shopId", SHOP_ID_COLUMN);
                        put("categoryId", CATEGORY_INT_ID_COLUMN);
                        put("recoProducts", "recoProducts");
                        put("categoryIntIds", "categoryIntIds");
                        put("recoProdShopId", "recoProdShopId");
                        put("recoProdScore", "recoProdScore");
                        put("inputTable", inputTableName);
                    }
                }))
                .map((MapFunction<Row, Row>) row -> reduceStatisticCategoryRecommendation(row,
                        RECO_SUGGESTED_PRODUCT_LIMIT,
                        Constants.getType(inputTableName)),
                        RowEncoder.apply(new StructType()
                                .add(SHOP_ID_COLUMN, DataTypes.IntegerType)
                                .add(CATEGORY_INT_ID_COLUMN, DataTypes.IntegerType)
                                .add("recoProducts", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdScore", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdShopId", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("type", DataTypes.StringType)))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(STATISTIC_CATEGORY_RECOMMENDATION_TABLE);
        return Collections.singletonList(STATISTIC_CATEGORY_RECOMMENDATION_TABLE);
    }

    private static Row reduceStatisticCategoryRecommendation(Row row, Long limit, String type) {
        final List<Row> list = row.getList(2);
        return list.stream().map(r ->
                new AbstractMap.SimpleImmutableEntry<Triple<Integer, Integer, Integer>, Integer>(Triple.of(r.getInt(0),
                        r.getInt(1),
                        r.getInt(2)),
                        r.getInt(3)))
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(AbstractMap.SimpleImmutableEntry<Triple<Integer, Integer, Integer>, Integer>::getKey,
                        LinkedHashMap<Triple<Integer, Integer, Integer>, Integer>::new,
                        Collectors.summingInt(AbstractMap.SimpleImmutableEntry::getValue)),
                        pairIntegerLinkedHashMap -> {
                            LinkedHashMap<Triple<Integer, Integer, Integer>, Integer> map = pairIntegerLinkedHashMap.entrySet().stream()
                                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                                    .limit(limit)
                                    .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                                            Integer::sum,
                                            LinkedHashMap::new));
                            return RowFactory.create(row.getInt(0),
                                    row.getInt(1),
                                    map.keySet().stream().map(Triple::getLeft).collect(toList()).toArray(),
                                    map.values().toArray(),
                                    map.keySet().stream().map(Triple::getRight).collect(toList()).toArray(),
                                    type);
                        }));
    }

    public static List<String> statisticCategoryStatRecommendation(SparkSession spark, String inputTableName, String path) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT " +
                        "ARRAYS_ZIP(%(categoryIntIds), " +
                        "%(recoProdShopId), %(recoProducts)) AS combinedData " +
                        "FROM %(inputTable)",
                new HashMap<String, String>() {
                    {
                        put("categoryIntIds", "categoryIntIds");
                        put("recoProdShopId", "recoProdShopId");
                        put("recoProducts", "recoProducts");
                        put("inputTable", inputTableName);
                    }
                }))
                .flatMap((FlatMapFunction<Row, Row>) row -> reduceStatisticCategoryStatRecommendation(row.getList(0),
                        RECO_SUGGESTED_PRODUCT_LIMIT,
                        Constants.getType(inputTableName)).iterator(),
                        RowEncoder.apply(new StructType()
                                .add(SHOP_ID_COLUMN, DataTypes.IntegerType)
                                .add(CATEGORY_INT_ID_COLUMN, DataTypes.IntegerType)
                                .add("productIntCatCount", DataTypes.IntegerType)
                                .add("type", DataTypes.StringType)))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(STATISTIC_CATEGORY_STAT_RECOMMENDATION_TABLE);


        return Collections.singletonList(STATISTIC_CATEGORY_STAT_RECOMMENDATION_TABLE);

    }

    private static List<Row> reduceStatisticCategoryStatRecommendation(List<Row> rows, Long limit, String type) {
        return rows.stream().map(r ->
                new AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>(Pair.of(r.getInt(0),
                        r.getInt(1)),
                        r.getInt(2)))
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>::getKey,
                        Collectors.toSet()),
                        pairIntegerLinkedHashMap ->
                                pairIntegerLinkedHashMap.entrySet().stream()
                                    .map(entry -> new AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>(entry.getKey(), entry.getValue().size()))
                                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                                    .limit(limit)
                                    .map(map -> RowFactory.create(map.getKey().getRight(),
                                            map.getKey().getLeft(),
                                            map.getValue(),
                                            type))
                                    .collect(toList())));
    }

    public static List<String> generateSingleFileStatistics(SparkSession spark, String path) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT " +
                        "s.%(shopId), s.%(categoryIntId), s.%(productIntCatCount), p.%(totalProductCount), s.%(type) " +
                        "FROM %(statisticTable) AS s " +
                        "INNER JOIN %(productStatTable) AS p " +
                        "ON s.%(shopId) = p.%(shopId) AND s.%(categoryIntId) = p.%(categoryIntId)",
                new HashMap<String, String>(){
                    {
                        put("shopId", SHOP_ID_COLUMN);
                        put("categoryIntId", CATEGORY_INT_ID_COLUMN);
                        put("productIntCatCount", "productIntCatCount");
                        put("totalProductCount", "totalProductCount");
                        put("type", "type");
                        put("statisticTable", STATISTIC_CATEGORY_STAT_RECOMMENDATION_TABLE);
                        put("productStatTable", PRODUCT_CATEGORY_STAT_TABLE);
                    }
                }))
                .coalesce(1)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(STATISTIC_CATEGORY_STAT_RECOMMENDATION);
        spark.sql("select * from " + STATISTIC_CATEGORY_RECOMMENDATION_TABLE)
                .coalesce(1)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(STATISTIC_CATEGORY_RECOMMENDATION);
        spark.sql("select * from " + STATISTIC_RECOMMENDATION_TABLE)
                .coalesce(1)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(STATISTIC_RECOMMENDATION);
        return Collections.singletonList(STATISTIC_CATEGORY_STAT_RECOMMENDATION_TABLE);

    }
}
