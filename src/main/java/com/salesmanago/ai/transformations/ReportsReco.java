package com.salesmanago.ai.transformations;

import com.salesmanago.ai.basicData.helpers.productRecoVisitedTogether.ProductMap;
import com.salesmanago.ai.utils.ScoringFunctions;
import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.commons.lang3.tuple.Pair;
import org.apache.commons.lang3.tuple.Triple;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;
import org.apache.spark.storage.StorageLevel;

import java.util.*;
import java.util.stream.Collectors;

import static com.salesmanago.ai.utils.Constants.*;
import static java.util.stream.Collectors.toMap;
import static org.apache.spark.api.java.StorageLevels.MEMORY_AND_DISK;

public class ReportsReco {

    public static List<String> buildRecoContactTable(SparkSession spark, String inputTable, String outputTable) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT cm.%(contactId), " +
                        "FLATTEN(COLLECT_LIST(ARRAYS_ZIP(rs.%(recoProducts), " +
                        "rs.%(recoProdShopId), " +
                        "rs.%(recoProdScore)))) AS combinedData " +
                        "FROM %(contactModel) AS cm " +
                        "INNER JOIN %(recoSuggestedVisitedTogether) AS rs " +
                        "ON rs.%(productId) = cm.%(productId) AND rs.%(shopId) = cm.%(shopId) " +
                        "GROUP BY cm.%(contactId)",
                new HashMap<String, String>() {
                    {
                        put("contactModel", GLOBAL_CONTACT_MODEL_TABLE);
                        put("recoSuggestedVisitedTogether", inputTable);
                        put("contactId", CONTACT_ID_COLUMN);
                        put("productId", PRODUCT_ID_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("recoProducts", "recoProducts");
                        put("recoProdScore", "recoProdScore");
                        put("recoProdShopId", "recoProdShopId");
                    }
                }))
                .map((MapFunction<Row, Row>) row ->
                                ScoringFunctions
                                        .reduceProductShopScoreToSingleRow(row.getList(1), row.getString(0), RECO_SUGGESTED_PRODUCT_LIMIT),
                        RowEncoder.apply(new StructType()
                                .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                                .add("recoProducts", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdScore", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdShopId", DataTypes.createArrayType(DataTypes.IntegerType))))
                .coalesce(1)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(outputTable);
        return Collections.singletonList(outputTable);
    }

    public static List<String> buildRecoSuggestedProductTable(SparkSession spark, String tableName) {
        spark.sql("select * from " + SUGGESTED_PRODUCTS_TO_REDUCE_TABLE)
                .persist(MEMORY_AND_DISK)
                .map((MapFunction<Row, Row>) ReportsReco::reduceToRecoSugestedProduct,
                        RowEncoder.apply(new StructType()
                                .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                                .add(SHOP_ID_COLUMN, DataTypes.IntegerType)
                                .add(CATEGORY_INT_ID_COLUMN, DataTypes.IntegerType)
                                .add("recoProducts", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdScore", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("recoProdShopId", DataTypes.createArrayType(DataTypes.IntegerType))
                                .add("categoryIntIds", DataTypes.createArrayType(DataTypes.IntegerType))))
                .persist(StorageLevel.MEMORY_AND_DISK())
                .coalesce(1)
                .sort(PRODUCT_ID_COLUMN)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(tableName);
        return Collections.singletonList(tableName);
    }

    public static Row reduceToRecoSugestedProduct(Row row) {
        final List<Row> list = row.getList(3);
        final HashMap<Triple<Integer, Integer, Integer>, Integer> reducedRecomendations =
                list.stream()
                        .reduce(new HashMap<Triple<Integer, Integer, Integer>, Integer>(),
                                (acc, r) -> {
                                    acc.compute(Triple.of(r.getInt(0), r.getInt(2), r.getInt(3))
                                    , (key, val) -> Objects.nonNull(val)
                                            ? val + r.getInt(1)
                                            : r.getInt(1));
                                    return acc;
                                },
                                (a, b) -> {
                                    b.forEach((key, value) ->
                                            a.put(key, a.getOrDefault(key, 0) + value));
                                    return a;
                                })
                        .entrySet()
                        .stream()
                        .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                        .limit(RECO_SUGGESTED_PRODUCT_LIMIT)
                        .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                                Integer::sum,
                                LinkedHashMap::new));
        return RowFactory.create(row.getInt(0),
                row.getInt(1),
                row.getInt(2),
                reducedRecomendations.keySet().stream()
                        .map(Triple::getLeft).toArray(),
                reducedRecomendations.values().toArray(),
                reducedRecomendations.keySet().stream()
                        .map(Triple::getMiddle).toArray(),
                reducedRecomendations.keySet().stream()
                        .map(Triple::getRight).toArray());
    }

    public static List<String> groupByProductAndAggregate(SparkSession spark) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT %(productId), %(shopId), %(categoryIntId), " +
                        "ARRAYS_ZIP(" +
                        "FLATTEN(COLLECT_LIST(%(recoPids))), " +
                        "FLATTEN(COLLECT_LIST(%(recoScores))), " +
                        "FLATTEN(COLLECT_LIST(%(recoShops))), " +
                        "FLATTEN(COLLECT_LIST(%(categoryIntIds)))) AS %(zipped) " +
                        "FROM %(suggestedNotGrouped) " +
                        "GROUP BY %(productId), %(shopId), %(categoryIntId)",
                new HashMap<String, String>() {
                    {
                        put("productId", PRODUCT_ID_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("categoryIntId", CATEGORY_INT_ID_COLUMN);
                        put("recoPids", "recoPids");
                        put("recoScores", "recoScores");
                        put("recoShops", "recoShops");
                        put("categoryIntIds", "categoryIntIds");
                        put("zipped", "zipped");
                        put("suggestedNotGrouped", SUGGESTED_PRODUCTS_NOT_GROUPED_TABLE);
                    }
                }))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(SUGGESTED_PRODUCTS_TO_REDUCE_TABLE);
        return Collections.singletonList(SUGGESTED_PRODUCTS_TO_REDUCE_TABLE);
    }

    public static List<String> generateSuggestedProductStructure(SparkSession spark,
                                                                 String scoringType,
                                                                 String contactInfo) {
        groupByContactInformationdWithZippedColumns(spark,
                contactInfo,
                whereClauseDispatcher(scoringType, contactInfo))
                .flatMap((FlatMapFunction<Row, Row>) row -> {
                    final List<Row> productTypeDate = row.getList(1);
                    final List<Row> collect = reduceToRecoStructure(productTypeDate, scoringType);
                    return collect.iterator();
                }, RowEncoder.apply(new StructType()
                        .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                        .add("recoPids", DataTypes.createArrayType(DataTypes.IntegerType))
                        .add("recoScores", DataTypes.createArrayType(DataTypes.IntegerType))
                        .add("recoShops", DataTypes.createArrayType(DataTypes.IntegerType))
                        .add("categoryIntIds", DataTypes.createArrayType(DataTypes.IntegerType))
                        .add(CATEGORY_INT_ID_COLUMN, DataTypes.IntegerType)
                        .add(SHOP_ID_COLUMN, DataTypes.IntegerType)))
                .write()
                .option("compression", "snappy")
                .option("parquet.block.size", PARQUET_BLOCK_SIZE)
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(SUGGESTED_PRODUCTS_NOT_GROUPED_TABLE);
        return Collections.singletonList(SUGGESTED_PRODUCTS_NOT_GROUPED_TABLE);
    }

    public static Dataset<Row> groupByContactInformationdWithZippedColumns(SparkSession spark, String contactInfo, String whereClause) {
        return spark.sql(SparkSqlHelper.replaceParams("SELECT %(contactInfo), "
                        + "ARRAYS_ZIP(COLLECT_LIST(%(productId)), "
                        + "COLLECT_LIST(%(date)), "
                        + "COLLECT_LIST(%(visitType)), "
                        + "COLLECT_LIST(%(shopId)), "
                        + "COLLECT_LIST(%(categoryIntId))) AS combinedData "
                        + "FROM %(productVisitTable) "
                        + "%(whereClause) "
                        + "GROUP BY %(contactInfo)",
                new HashMap<String, String>() {
                    {
                        put("contactInfo", contactInfo);
                        put("productId", PRODUCT_ID_COLUMN);
                        put("date", DATE_COLUMN);
                        put("visitType", VISIT_TYPE_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("categoryIntId", CATEGORY_INT_ID_COLUMN);
                        put("productVisitTable", PRODUCT_VISIT_TABLE);
                        put("whereClause", whereClause);
                    }
                }));
    }

    public static List<Row> reduceToRecoStructure(List<Row> productTypeDate,
                                                  String scoringType) {
        return productTypeDate.stream()
                .map(currentProduct -> {
                    final ProductMap map = productTypeDate.stream()
                            .filter(p -> p.getInt(0) != currentProduct.getInt(0))
                            .reduce(new ProductMap(currentProduct.getInt(0), currentProduct.getInt(3), currentProduct.getInt(4)),
                                    (acc, element) ->
                                            acc.addToMap(element.getInt(0),
                                                    scorePairOfEvents(Pair.of(element, currentProduct), scoringType),
                                                    element.getInt(3),
                                                    element.getInt(4)),
                                    (p1, p2) ->
                                            p1.mergeMaps(p2.getMap()));
                    return RowFactory.create(map.getProductId(),
                            !map.getMap().keySet().isEmpty()
                                    ? map.getMap().keySet().toArray()
                                    : null,
                            map.getScores().size() > 0
                                    ? map.getScores().toArray()
                                    : null,
                            map.getShopIds().toArray(),
                            map.getCategories().toArray(),
                            map.getCategoryIntId(),
                            map.getShopId());
                })
                .filter(r -> !r.isNullAt(1) && !r.isNullAt(2))
                .collect(Collectors.toList());
    }

    private static Integer scorePairOfEvents(Pair<Row, Row> rowPair, String type) {
        switch (type) {
            case "bv":
                return ScoringFunctions.scoreBoughtAfterVisit(rowPair);
            case "mw":
                return ScoringFunctions.scoreMixedWeight(rowPair);
            case "bt":
                return ScoringFunctions.scoreBoughtTogether(rowPair);
            case "vt":
            default:
                return ScoringFunctions.scoreVisitedTogether(rowPair);
        }
    }

    private static String whereClauseDispatcher(String scoringType, String contactInfo) {
        switch (scoringType) {
            case "vt":
            case "bt":
                return SparkSqlHelper.recoGroupingWhereClause(contactInfo, scoringType);
            case "mw":
            case "bv":
            default:
                return SparkSqlHelper.recoGroupingWhereClause(contactInfo);
        }
    }

}
