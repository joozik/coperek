package com.salesmanago.ai.transformations;

import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.salesmanago.ai.basicData.BasicStructures.EXPORT_VISIT_STRUCT_WITH_SCORE;
import static com.salesmanago.ai.utils.Constants.*;


public class TransactionsExtended {
    public static List<String> buildProductVisitsFromTransactions(SparkSession spark) {
        spark.sql(SparkSqlHelper
                .replaceParams("SELECT CAST(p.%(productId) AS int) %(productId), p.%(uniqueId), p.%(category), "
                                + "p.%(categoryIntId), CAST(p.%(shopId) AS int) %(shopId), t.%(contactId), '' AS %(uuIdIntId), "
                                + "t.%(transactionDate) AS %(date), '%(purchaseVisitType)' AS %(visitType), p.%(url), "
                                + "'' AS %(urlParameters) "
                                + "FROM %(transactionTable) AS t "
                                + "INNER JOIN %(productTable) AS p "
                                + "ON t.%(productExtId) = p.%(productExtId)",
                        new HashMap<String, String>() {
                            {
                                put("productId", PRODUCT_ID_COLUMN);
                                put("uniqueId", PRODUCT_UNIQUE_ID_COLUMN);
                                put("category", CATEGORY_COLUMN);
                                put("categoryIntId", CATEGORY_INT_ID_COLUMN);
                                put("shopId", SHOP_ID_COLUMN);
                                put("contactId", CONTACT_ID_COLUMN);
                                put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                                put("date", DATE_COLUMN);
                                put("visitTypeConst", VIEW_VISIT_TYPE);
                                put("visitType", VISIT_TYPE_COLUMN);
                                put("url", URL_COLUMN);
                                put("urlParameters", URL_PARAMETERS_COLUMN);
                                put("productTable", PRODUCT_TABLE);
                                put("transactionDate", TRANSACTION_DATE_COLUMN);
                                put("purchaseVisitType", PURCHASE_VISIT_TYPE);
                                put("transactionTable", TRANSACTION_TABLE);
                                put("productExtId", PRODUCT_EXT_ID_COLUMN);
                            }
                        }))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(PRODUCT_VISIT_TABLE);
        return Collections.singletonList(PRODUCT_VISIT_TABLE);
    }

    public static Dataset<Row> buildExtractedVisits(SparkSession spark) {
        return spark.sql(SparkSqlHelper.replaceParams("SELECT %(date), %(contactId), "
                        + "%(visitType), %(productId), %(shopId), %(uniqueId), "
                        + "%(pScoreConst) AS %(score) "
                        + "FROM %(productVisitTable) "
                        + "WHERE %(visitType) = '%(purchaseVisitType)'",
                new HashMap<String, String>() {
                    {
                        put("date", DATE_COLUMN);
                        put("contactId", CONTACT_ID_COLUMN);
                        put("productId", PRODUCT_ID_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("visitType", VISIT_TYPE_COLUMN);
                        put("purchaseVisitType", PURCHASE_VISIT_TYPE);
                        put("pScoreConst", "50");
                        put("score", SCORE_COLUMN);
                        put("uniqueId", PRODUCT_UNIQUE_ID_COLUMN);
                        put("productVisitTable", PRODUCT_VISIT_TABLE);
                    }
                }));
    }
}
