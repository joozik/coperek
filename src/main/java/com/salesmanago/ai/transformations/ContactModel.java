package com.salesmanago.ai.transformations;

import com.salesmanago.ai.utils.ScoringFunctions;
import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.*;

import static com.salesmanago.ai.utils.Constants.*;

public class ContactModel {
    public static List<String> buildGlobalContactModel(SparkSession spark) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT vt.%(contactId), ARRAYS_ZIP("
                        + "COLLECT_LIST(vt.%(productId)), "
                        + "COLLECT_LIST(vt.%(shopId)), "
                        + "COLLECT_LIST(vt.%(score))) AS %(combinedData) "
                        + "FROM %(visitTransactions) vt "
                        + "WHERE vt.%(contactId) IS NOT NULL "
                        + "GROUP BY %(contactId)",
                new HashMap<String, String>() {
                    {
                        put("productId", PRODUCT_ID_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("score", SCORE_COLUMN);
                        put("combinedData", COMBINED_DATA_COLUMN);
                        put("visitTransactions", VISITS_AND_TRANSACTIONS_TABLE);
                        put("contactId", CONTACT_ID_COLUMN);
                    }
                }))
                .flatMap((FlatMapFunction<Row, Row>) row ->
                                ScoringFunctions
                                        .reduceProducShopScoreToListOfRows(row.getList(1), row.getString(0), 10)
                                        .iterator(),
                        RowEncoder.apply(new StructType()
                                .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                                .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                                .add(SCORE_COLUMN, DataTypes.IntegerType)
                                .add(SHOP_ID_COLUMN, DataTypes.IntegerType)))
                .coalesce(1)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(GLOBAL_CONTACT_MODEL_TABLE);
        return Collections.singletonList(GLOBAL_CONTACT_MODEL_TABLE);
    }

}

