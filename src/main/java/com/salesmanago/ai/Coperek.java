package com.salesmanago.ai;

import com.salesmanago.ai.actions.JavaALSExample;
import com.salesmanago.ai.executionPaths.GlobalExecutionPath;
import com.salesmanago.ai.utils.FileUtils;
import org.apache.spark.sql.*;

import java.util.HashMap;

import static com.salesmanago.ai.utils.Constants.*;

public class Coperek {

    public static void main(String[] args) {
//        final String PATH = "/home/joozik/copernicus/preparedData2/coperek/";
//        final String PATH = "/home/joozik/copernicus/preparedData2/coperek-mm/";
        final String PATH = "/home/joozik/copernicus/preparedData2/coperek-noble/";
//        final String PATH = "/home/joozik/copernicus/data_noble/";
//        final String PATH = "/home/joozik/copernicus/data_media_markt/";

        FileUtils.removeDirectoryIfExist(PATH + HIVE_TABLE_DIR);
        FileUtils.removeDirectoryIfExist(PATH + "myCollaborativeFilter");
        FileUtils.removeDirectoryIfExist(PATH + "checkpoints");
        FileUtils.removeDirectoryIfExist("metastore_db");
        FileUtils.createDirectory(PATH + HIVE_TABLE_DIR);


        SparkSession spark = Coperek.sparkSession("local[*]", "Coperek", PATH + HIVE_TABLE_DIR);

        final GlobalExecutionPath globalExecutionPath = new GlobalExecutionPath(spark, PATH);

        final HashMap<String, Boolean> tablesFromFirstStep = globalExecutionPath.firstStep_prepareAndFormData();
        spark.sql("drop table "+ VISIT_TABLE);
        spark.sql("drop table "+ VISIT_FILTERED_TABLE);

        final HashMap<String, Boolean> tablesFromSecondStep = globalExecutionPath.secondStep_createRecoSuggestedProductsVisitedTogether();
        tablesFromSecondStep.forEach((tablename, flag) -> spark.sql("drop table " + tablename));

        final HashMap<String, Boolean> tablesFromThirdStep = globalExecutionPath.thirdStep_createRecoSuggestedProductsBoughtAfterVisit();
        tablesFromThirdStep.forEach((tablename, flag) -> spark.sql("drop table " + tablename));

        final HashMap<String, Boolean> tablesFromFourthStep = globalExecutionPath.fourthStep_createRecoSuggestedProductsMixedWeight();
        tablesFromFourthStep.forEach((tablename, flag) -> spark.sql("drop table " + tablename));

        final HashMap<String, Boolean> tablesFromFifthStep = globalExecutionPath.fifthStep_createRecoSuggestedProductsBoughtTogether();
        tablesFromFifthStep.forEach((tablename, flag) -> spark.sql("drop table " + tablename));

        globalExecutionPath.sixthStep_generateSingleFileStatistics();
    }

    public static SparkSession sparkSession(String master,
                                            String appName,
                                            String path) {
        return SparkSession.builder()
                .appName(appName)
                .master(master)
                .config("spark.dynamicAllocation.enabled", true)
                .config("spark.shuffle.service.enabled", true)
                .config("spark.cores.max", "10")
                .config("spark.sql.warehouse.dir", path)
                .config("spark.sql.inMemoryColumnarStorage.compressed", true)
                .config("hive.execution.engine", "spark")
                .enableHiveSupport()
                .getOrCreate();
    }

}

