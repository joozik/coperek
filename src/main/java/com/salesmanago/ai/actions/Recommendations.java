package com.salesmanago.ai.actions;

// $example on$

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.rdd.RDD;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
import scala.Tuple2;

import org.apache.spark.api.java.*;
import org.apache.spark.mllib.recommendation.ALS;
import org.apache.spark.ml.recommendation.ALSModel;
import org.apache.spark.mllib.recommendation.MatrixFactorizationModel;
import org.apache.spark.mllib.recommendation.Rating;
import org.apache.spark.SparkConf;
import scala.reflect.ClassTag;

import java.util.Collections;
import java.util.List;
// $example off$

public class Recommendations {
    private SparkSession spark;
    private String path;

    public Recommendations(SparkSession sparkSession, String path) {

    }

    public static void sparkRecommendations(SparkSession spark, String path) {
        // Build the recommendation model using ALS on the training data
//        org.apache.spark.ml.recommendation.ALS als = new org.apache.spark.ml.recommendation.ALS()
//                .setMaxIter(5)
//                .setRegParam(0.01)
//                .setUserCol("userId")
//                .setItemCol("movieId")
//                .setRatingCol("rating");
        //>>>>>>>>tego nie ma
//        val vendorId = args(0)
//        val inputFile = args(1) <- mahout-data
//        val productsFile = args(2) <- products.csv
//        val modelOutputDir = args(3) <- myColaborativeFilter
//        val recOutputDir = args(4) <- suggested_cf_user
//        val appId = args(5) <- 2
//        val versionId = args(6) <- 1
        //<<<<<<<<<tego nie ma
        // $example on$

        spark.sparkContext().setCheckpointDir(path + "checkpoints");

        // Load and parse the data
        JavaRDD<Rating> ratings =
                spark.sql("select contactId, productUniqueId, score from mahout_data")
                        .toJavaRDD()
                        .map(row -> new Rating(row.getInt(0),
                                row.getInt(1),
                                row.getFloat(2)));
//        JavaRDD products = sc.textFile(productsFile).map { line =>
//            val fields = line.split("\\|")
//            (fields(0).toInt, fields(5))
//        }.collect.toMap
//
//        val numRatings = ratings.count
//        val numUsers = ratings.map(_.user).distinct.count
//        val numProducts = ratings.map(_.product).distinct.count

        // Build the recommendation model using ALS
        int rank = 20;
        int numIterations = 20;
        MatrixFactorizationModel model = ALS.train(JavaRDD.toRDD(ratings), rank, numIterations, 0.01);
        // Evaluate the model on rating data
        JavaRDD<Tuple2<Object, Object>> userProducts =
                ratings.map(r -> new Tuple2<Object, Object>(r.user(), r.product()));
        JavaPairRDD<Tuple2<Integer, Integer>, Double> predictions = JavaPairRDD.fromJavaRDD(
                model.predict(JavaRDD.toRDD(userProducts)).toJavaRDD()
                        .map(r -> new Tuple2<>(new Tuple2<>(r.user(), r.product()), r.rating()))
        );
        JavaRDD<Tuple2<Double, Double>> ratesAndPreds = JavaPairRDD.fromJavaRDD(
                ratings.map(r -> new Tuple2<>(new Tuple2<>(r.user(), r.product()), r.rating())))
                .join(predictions).values();
        double MSE = ratesAndPreds.mapToDouble(pair -> {
            double err = pair._1() - pair._2();
            return err * err;
        }).mean();
        System.out.println("Mean Squared Error = " + MSE);

        // Save and load model
        model.save(spark.sparkContext(), path + "myCollaborativeFilter");
//        model.save(spark.sc(), "target/tmp/myCollaborativeFilter");
        MatrixFactorizationModel sameModel = MatrixFactorizationModel.load(spark.sparkContext(),
                path + "myCollaborativeFilter");
//        final JavaRDD<Tuple2<Object, Rating[]>> tuple2JavaRDD = sameModel.recommendProductsForUsers(20).toJavaRDD();
//        tuple2JavaRDD
//                .map(row -> {
//            System.out.println("row _1: " + row._1());
//            final Rating[] ratings1 = row._2();
//            System.out.println("row _2: " + ratings1);
//            System.out.println("ratings1[0]: " + ratings1[0]);
//            System.out.println("ratings1[1]: " + ratings1[1]);
//            System.out.println("ratings1[2]: " + ratings1[2]);
//            return row._1().toString();
//        }).saveAsTextFile(path + "dupskooooo");
        // $example off$

//        spark.stop();
    }

    public static List<String> formerMahout() {
//        mahout spark-itemsimilarity --input /media/konrad/data/noble/extract/ --output /media/konrad/data/noble/mahout-out --filter1 purchase --filter2 view --itemIDColumn 3 --rowIDColumn 1 --filterColumn 2 --inDelim , --filenamePattern v.*
        return Collections.singletonList("");
    }
}

/*


    sc.setCheckpointDir("/media/konrad/data/checkpoints")

    // Load and parse the data
    val data = sc.textFile(inputFile)
    val ratings = data.map(_.split(',') match { case Array(user, item, rate) =>
      Rating(user.toInt, item.toInt, rate.toDouble)
    })

  //>>>>>>>>tego nie ma
    val products = sc.textFile(productsFile).map { line =>
      val fields = line.split("\\|")
      (fields(0).toInt, fields(5))
    }.collect.toMap

    val numRatings = ratings.count
    val numUsers = ratings.map(_.user).distinct.count
    val numProducts = ratings.map(_.product).distinct.count

    println("## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ")
    println("# Got " + numRatings + " ratings from " + numUsers + " users on " + numProducts + " products.")
    println("## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ")
  //<<<<<<<<<tego nie ma

    // Build the recommendation model using ALS
    val rank = 20 //original 10
    val numIterations = 20 // original 10
    val model = ALS.train(ratings, rank, numIterations, 0.01)

    // Save and load model
    model.save(sc, modelOutputDir)
    val sameModel = MatrixFactorizationModel.load(sc, modelOutputDir)

    // Evaluate the model on rating data
    val usersProducts = ratings.map { case Rating(user, product, rate) =>
      (user, product)
    }
    val predictions =
      sameModel.predict(usersProducts).map { case Rating(user, product, rate) =>
        ((user, product), rate)
      }
    val ratesAndPreds = ratings.map { case Rating(user, product, rate) =>
      ((user, product), rate)
    }.join(predictions)
    val MSE = ratesAndPreds.map { case ((user, product), (r1, r2)) =>
      val err = (r1 - r2)
      err * err
    }.mean()

    println("## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ")
    println("# Mean Squared Error = " + MSE)
    println("## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ")

    //    println("## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ")
    //    var reco = sameModel.recommendProducts(1123521, 10)
    //    println("## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ## ")

    val recs = model.recommendProductsForUsers(20)
    //    recs.mapValues(mkStr(_)).saveAsTextFile(recOutputDir)
    recs.map(mkStr(_, products, vendorId, appId, versionId)).saveAsTextFile(recOutputDir)

    sc.stop()
  }

  //>>>>>>>>tego nie ma
  //mkString(",").replace("Rating", "")
  def mkStr(e: (Int, Array[Rating]), products: Map[Int, String], vendorId: String, appId: String, versionId: String): String = {
    var pStr = "";
    e._2.foreach(x => pStr += "," + x.product + "")

    var wStr = "";
    e._2.foreach(x => wStr += "," + x.rating + "")

    appId + "|" + vendorId + "|" + e._1 + "|c|" + versionId + "|{" + pStr.replaceFirst("\\,", "") + "}" + "|{" + wStr.replaceFirst("\\,", "") + "}"
  }
  //<<<<<<<<<tego nie ma
}

// scalastyle:on println



echo "Running Spark recommendations"
export SPARK_HOME=/media/konrad/work/tools/spark-2.2.0-bin-hadoop2.7
/media/konrad/work/tools/spark-2.2.0-bin-hadoop2.7/bin/spark-submit --class "RecommendationExample" --master local[4] --executor-memory 32G /media/konrad/work/data-processor/spark/recommendations/target/scala-2.11/spark-recommendations_2.11-1.0.jar 4249 /media/konrad/data/noble/extract/mahout-data.csv /media/konrad/data/noble/data/products.csv /media/konrad/data/noble/extract/myCollaborativeFilter /media/konrad/data/noble/reports/suggested_cf_user 2 1

echo "Running Mahout recommendations"
export SPARK_HOME=/media/konrad/work/tools/spark-1.6.3-bin-hadoop2.4
/media/konrad/work/tools/apache-mahout-distribution-0.13.0/bin/mahout spark-itemsimilarity --input /media/konrad/data/noble/extract/ --output /media/konrad/data/noble/mahout-out --filter1 purchase --filter2 view --itemIDColumn 3 --rowIDColumn 1 --filterColumn 2 --inDelim , --filenamePattern v.*

echo "Generating CSV files from Mahout"
./run.sh "noble" "-mahout"



echo "Importing: /home/tomcat/script2/$1/mahout-out/similarity-matrix.csv"
/usr/local/psql/bin/psql -h "10.205.204.57" -d "managopg" -U "bi_user" -c "\copy bi.cp_product_recommendations (app_id, vendor_id, product_id, rtype, version, reco, rating, product_vendor_data_source_id, product_category_id)  from '/home/tomcat/script2/$1/mahout-out/similarity-matrix.csv' with delimiter as '|'";


​
for x in $(ls /home/tomcat/script2/$1/reports/suggested_cf_user/part-0*);
do
  echo "Importing: $x"
  /usr/local/psql/bin/psql -h "10.205.204.57" -d "managopg" -U "bi_user" -c "\copy bi.cp_new_recommendations (app_id, vendor_id, contact_id, rtype, version, reco, rating)  from '$x' with delimiter as '|'";
done
 */
