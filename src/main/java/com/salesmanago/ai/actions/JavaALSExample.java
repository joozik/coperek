package com.salesmanago.ai.actions;

import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;


import org.apache.spark.ml.evaluation.RegressionEvaluator;
import org.apache.spark.ml.recommendation.ALS;
import org.apache.spark.ml.recommendation.ALSModel;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.salesmanago.ai.utils.Constants.*;

public class JavaALSExample {

    public static List<String> sparkRawRecommendations(SparkSession spark, String path) {
        spark.sparkContext().setCheckpointDir(path + "checkpoints");
        Dataset<Row> ratings = spark.sql("select contactId, productUniqueId, score from mahout_data");
        Dataset<Row>[] splits = ratings.randomSplit(new double[]{0.8, 0.2});
        Dataset<Row> training = splits[0];
        Dataset<Row> test = splits[1];

        ALS als = new ALS()
                .setMaxIter(20)
                .setRank(20)
                .setRegParam(0.01)
                .setUserCol("contactId")
                .setItemCol("productUniqueId")
                .setRatingCol("score");
        ALSModel model = als.fit(training);

        model.setColdStartStrategy("drop");
        Dataset<Row> predictions = model.transform(test);

        RegressionEvaluator evaluator = new RegressionEvaluator()
                .setMetricName("rmse")
                .setLabelCol("score")
                .setPredictionCol("prediction");
        double rmse = evaluator.evaluate(predictions);
        System.out.println("Root-mean-square error = " + rmse);

        Dataset<Row> userRecs = model.recommendForAllUsers(20);
        userRecs.
                flatMap((FlatMapFunction<Row, Row>) row -> {
                            final List<Row> list = row.getList(1);
                            return list.stream()
                                    .map(r -> RowFactory.create(row.getInt(0), r.getInt(0), r.getFloat(1)))
                                    .iterator();
                        },
                        RowEncoder.apply(new StructType()
                                .add("contactId", DataTypes.IntegerType)
                                .add("productId", DataTypes.IntegerType)
                                .add("score", DataTypes.FloatType)))
                .write()
                .mode(SaveMode.Append)
                .format("parquet")
                .saveAsTable(SPARK_ALS_RAW_TABLE);
        return Collections.singletonList(SPARK_ALS_RAW_TABLE);
    }

    public static List<String> joinWithProductGroupByContact(SparkSession spark) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT s.%(contactId), COLLECT_LIST(p.%(productId)) AS %(productId), "
                        + "COLLECT_LIST(s.%(score)) AS %(score), COLLECT_LIST(p.%(shopId)) AS %(shopId) "
                        + "FROM %(sparkALSRawTable) AS s "
                        + "INNER JOIN %(productTable) AS p "
                        + "ON p.%(productUniqueId) = s.%(productId) "
                        + "GROUP BY s.%(contactId)",
                new HashMap<String, String>() {
                    {
                        put("contactId", CONTACT_ID_COLUMN);
                        put("productId", PRODUCT_ID_COLUMN);
                        put("score", SCORE_COLUMN);
                        put("shopId", SHOP_ID_COLUMN);
                        put("productUniqueId", PRODUCT_UNIQUE_ID_COLUMN);
                        put("productTable", PRODUCT_TABLE);
                        put("sparkALSRawTable", SPARK_ALS_RAW_TABLE);
                    }
                }))
                .coalesce(1)
                .write()
                .mode(SaveMode.Append)
                .format("json")
                .saveAsTable(SPARK_ALS_TABLE);
        return Collections.singletonList(SPARK_ALS_TABLE);
    }
}
