package com.salesmanago.ai.actions;

import com.salesmanago.ai.transformations.ProductsExtended;
import com.salesmanago.ai.transformations.TransactionsExtended;
import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

import static com.salesmanago.ai.basicData.BasicStructures.MAHOUT_DATA_STRUCT;
import static com.salesmanago.ai.utils.Constants.*;

public class ExtractAction {

    public static List<String> createBasicVisitTransactionTable(SparkSession spark) {
        TransactionsExtended
                .buildExtractedVisits(spark)
                .union(ProductsExtended.buildExtractedVisits(spark))
                .coalesce(1)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(VISITS_AND_TRANSACTIONS_TABLE);
        return Collections.singletonList(VISITS_AND_TRANSACTIONS_TABLE);
    }
    public static List<String> formerExtracted(String path,
                                               SparkSession spark) {
        spark.sql(SparkSqlHelper.replaceParams("SELECT %(date), %(contactId), %(visitType), %(uniqueId) "
                + "FROM %(visitAndTransaction) "
                + "WHERE %(contactId) IS NOT NULL",
                new HashMap<String, String>(){
                    {
                        put("date", DATE_COLUMN);
                        put("contactId", CONTACT_ID_COLUMN);
                        put("visitType",VISIT_TYPE_COLUMN);
                        put("uniqueId", PRODUCT_UNIQUE_ID_COLUMN);
                        put("visitAndTransaction", VISITS_AND_TRANSACTIONS_TABLE);
                    }
                }))
                .coalesce(1)
                .write()
                .mode(SaveMode.Append)
                .format("csv")
                .saveAsTable(EXTRACTED_VISITS_TABLE);

        spark.sql("select * from " + VISITS_AND_TRANSACTIONS_TABLE + " where "+ CONTACT_ID_COLUMN + " IS NOT NULL")
                .groupBy(CONTACT_ID_COLUMN, PRODUCT_UNIQUE_ID_COLUMN)
                .sum(SCORE_COLUMN)
                .map((MapFunction<Row, Row>) row -> {
                            return RowFactory.create(Integer.parseInt(row.getString(0)),
                                    row.getInt(1),
                                    (float) row.getLong(2) / 100);
                        },
                RowEncoder.apply(MAHOUT_DATA_STRUCT))
                .coalesce(1)
                .write()
                .mode(SaveMode.Append)
                .format("csv")
                .saveAsTable(MAHOUT_DATA_TABLE);

        return Arrays.asList(EXTRACTED_VISITS_TABLE,
                MAHOUT_DATA_TABLE);
    }
}

