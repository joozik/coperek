package com.salesmanago.ai.basicData;

import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import static com.salesmanago.ai.utils.Constants.*;

public class BasicStructures {
    public static final StructType EXPORT_VISIT_STRUCT_WITH_SCORE =
            new StructType()
                    .add(TRANSACTION_DATE_COLUMN, DataTypes.StringType)
                    .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                    .add(VISIT_TYPE_COLUMN, DataTypes.StringType)
                    .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                    .add(SHOP_ID_COLUMN, DataTypes.IntegerType)
                    .add(PRODUCT_UNIQUE_ID_COLUMN, DataTypes.IntegerType)
                    .add(SCORE_COLUMN, DataTypes.IntegerType);

    public static final StructType EXPORT_VISIT_STRUCT =
            new StructType()
                    .add(TRANSACTION_DATE_COLUMN, DataTypes.StringType)
                    .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                    .add(VISIT_TYPE_COLUMN, DataTypes.StringType)
                    .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType);

    public static final StructType MAHOUT_DATA_STRUCT =
            new StructType()
                    .add(CONTACT_ID_COLUMN, DataTypes.IntegerType)
                    .add(PRODUCT_UNIQUE_ID_COLUMN, DataTypes.IntegerType)
                    .add(SCORE_COLUMN, DataTypes.FloatType);

    public static final StructType REPORT_GENERAL_STRUCT =
            new StructType()
                    .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                    .add(PRODUCT_EXT_ID_COLUMN, DataTypes.StringType)
                    .add(CATEGORY_COLUMN, DataTypes.StringType)
                    .add(SUBCATEGORY_COLUMN, DataTypes.StringType)
                    .add(CATEGORY_INT_ID_COLUMN, DataTypes.StringType)
                    .add(SHOP_ID_COLUMN, DataTypes.IntegerType)
                    .add(URL_COLUMN, DataTypes.StringType)
                    .add(URL_PARAMETERS_COLUMN, DataTypes.StringType)
                    .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                    .add(UU_ID_INT_ID_COLUMN, DataTypes.StringType)
                    .add(DATE_COLUMN, DataTypes.StringType)
                    .add(VISIT_TYPE_COLUMN, DataTypes.StringType);

    public static final StructType REPORT_GENERAL_STRUCT_V2 =
            new StructType()
                    .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                    .add(CATEGORY_COLUMN, DataTypes.StringType)
                    .add(SUBCATEGORY_COLUMN, DataTypes.StringType)
                    .add(SHOP_ID_COLUMN, DataTypes.IntegerType)
                    .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                    .add(UU_ID_INT_ID_COLUMN, DataTypes.StringType)
                    .add(DATE_COLUMN, DataTypes.StringType)
                    .add(VISIT_TYPE_COLUMN, DataTypes.StringType);

    public static final StructType REPORT_GENERAL_GROUPED_BY_PRODUCT =
            new StructType()
                    .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                    .add(PRODUCT_EXT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(CATEGORY_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(SUBCATEGORY_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(CATEGORY_INT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(SHOP_ID_COLUMN, DataTypes.createArrayType(DataTypes.IntegerType))
                    .add(URL_COLUMN, DataTypes.StringType)
                    .add(URL_PARAMETERS_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(CONTACT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(UU_ID_INT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(DATE_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(VISIT_TYPE_COLUMN, DataTypes.createArrayType(DataTypes.StringType));

    public static final StructType REPORT_GENERAL_GROUPED_BY_CONTACT =
            new StructType()
                    .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                    .add(PRODUCT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(PRODUCT_EXT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(CATEGORY_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(SUBCATEGORY_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(CATEGORY_INT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(SHOP_ID_COLUMN, DataTypes.createArrayType(DataTypes.IntegerType))
                    .add(URL_COLUMN, DataTypes.StringType)
                    .add(URL_PARAMETERS_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(UU_ID_INT_ID_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(DATE_COLUMN, DataTypes.createArrayType(DataTypes.StringType))
                    .add(VISIT_TYPE_COLUMN, DataTypes.createArrayType(DataTypes.StringType));
}
