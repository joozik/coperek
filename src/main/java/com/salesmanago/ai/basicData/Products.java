package com.salesmanago.ai.basicData;

import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import static org.apache.spark.sql.functions.*;

import java.util.*;

import static com.salesmanago.ai.utils.Constants.*;

public class Products {
    public static List<String> buildProducts(String path, SparkSession spark) {
        spark.read()
                .option("header", "false")
                .option("delimiter", "|")
                .csv(path + PRODUCTS_FILE_NAME)
                .map((MapFunction<Row, Row>) row -> {
                    final String categories = row.getString(3);
                    List<String> category = new ArrayList<>();
                    if (Objects.nonNull(categories)) {
                        category.addAll(Arrays.asList(categories.split("/")));
                    }
                    String url = row.getString(4);
                    if (Objects.nonNull(url)) {
                        if (url.contains("?")) {
                            url = url.substring(0, url.lastIndexOf("?"));
                        }
                        if (url.contains("%3F")) {
                            url = url.substring(0, url.lastIndexOf("%3F"));
                        }
                        url = url.replaceFirst("^(http[s]?://)", "");
                    }
                    return RowFactory.create(Integer.valueOf(row.getString(0)),
                            row.getString(1),
                            row.getString(2),
                            category.size() > 0 ? category.get(0) : "",
                            category.size() > 1 ? category.get(1) : "",
                            url,
                            row.getString(7),
                            Integer.valueOf(row.getString(9)),
                            Integer.valueOf(row.getString(10)));
                }, RowEncoder.apply(new StructType()
                        .add(PRODUCT_ID_COLUMN, DataTypes.IntegerType)
                        .add(PRODUCT_EXT_ID_COLUMN, DataTypes.StringType)
                        .add(NAME_COLUMN, DataTypes.StringType)
                        .add(CATEGORY_COLUMN, DataTypes.StringType)
                        .add(SUBCATEGORY_COLUMN, DataTypes.StringType)
                        .add(URL_COLUMN, DataTypes.StringType)
                        .add(CREATED_ON_COLUMN, DataTypes.StringType)
                        .add(CATEGORY_INT_ID_COLUMN, DataTypes.IntegerType)
                        .add(SHOP_ID_COLUMN, DataTypes.IntegerType)))
                .withColumn(PRODUCT_UNIQUE_ID_COLUMN, monotonically_increasing_id().cast(DataTypes.IntegerType))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(PRODUCT_TABLE);

        spark.sql(SparkSqlHelper.replaceParams("SELECT %(shopId), %(categoryIntId), COUNT(%(productId)) AS %(total) " +
                        "FROM %(productTable) " +
                        "GROUP BY %(shopId), %(categoryIntId)",
                new HashMap<String, String>() {
                    {
                        put("shopId", SHOP_ID_COLUMN);
                        put("categoryIntId", CATEGORY_INT_ID_COLUMN);
                        put("productId", PRODUCT_ID_COLUMN);
                        put("total", "totalProductCount");
                        put("productTable", PRODUCT_TABLE);
                    }
                }))
                .coalesce(1)
                .write()
                .format("json")
                .mode(SaveMode.Append)
                .saveAsTable(PRODUCT_CATEGORY_STAT_TABLE);

        return Arrays.asList(PRODUCT_TABLE, PRODUCT_CATEGORY_STAT_TABLE);
    }
}
