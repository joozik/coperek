package com.salesmanago.ai.basicData.helpers.productRecoVisitedTogether;

import java.io.Serializable;

public class ProductRecoMetadata implements Serializable {
    private Integer score;
    private Integer shopId;
    private Integer categoryIntId;

    public ProductRecoMetadata() {
        this.score = 0;
        this.shopId = null;
        this.categoryIntId = null;
    }

    public ProductRecoMetadata(Integer shopId, Integer categoryIntId) {
        this.score = 0;
        this.shopId = shopId;
        this.categoryIntId = categoryIntId;
    }

    public Integer getScore() {
        return score;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Integer getCategoryIntId() {
        return categoryIntId;
    }

    public void setCategoryIntId(Integer category) {
        this.categoryIntId = category;
    }
}
