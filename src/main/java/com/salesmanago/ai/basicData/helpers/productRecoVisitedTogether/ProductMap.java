package com.salesmanago.ai.basicData.helpers.productRecoVisitedTogether;

import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

public class ProductMap implements Serializable {
    private Integer productId;
    private Integer shopId;
    private Integer categoryIntId;
    private Map<Integer, ProductRecoMetadata> map;

    public ProductMap(Integer productId, Integer shopId, Integer categoryIntId) {
        this.productId = productId;
        this.shopId = shopId;
        this.categoryIntId = categoryIntId;
        map = new HashMap<>();
    }

    public ProductMap(Integer productId, Map<Integer, ProductRecoMetadata> map) {
        this.productId = productId;
        this.map = map;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getShopId() {
        return shopId;
    }

    public void setShopId(Integer shopId) {
        this.shopId = shopId;
    }

    public Map<Integer, ProductRecoMetadata> getMap() {
        return map;
    }

    public Integer getCategoryIntId() {
        return categoryIntId;
    }

    public void setCategoryIntId(Integer categoryIntId) {
        this.categoryIntId = categoryIntId;
    }

    public void setMap(Map<Integer, ProductRecoMetadata> map) {
        this.map = map;
    }

    public ProductMap addToMap(Integer pid, Integer score, Integer shopId, Integer categoryIntId) {
        if (Objects.nonNull(score)) {
            ProductRecoMetadata productRecoMetadata = this.map.getOrDefault(pid,
                    new ProductRecoMetadata(shopId, categoryIntId));
            productRecoMetadata.setScore(productRecoMetadata.getScore() + score);
            this.map.put(pid, productRecoMetadata);
        }
        return this;
    }

    public ProductMap mergeMaps(Map<Integer, ProductRecoMetadata> map) {
        map.forEach((key, value) -> {
            ProductRecoMetadata productRecoMetadata = this.map.getOrDefault(key,
                    new ProductRecoMetadata(value.getShopId(), value.getCategoryIntId()));
            productRecoMetadata.setScore(productRecoMetadata.getScore() + value.getScore());
            this.map.put(key, productRecoMetadata);
        });
        return this;
    }

    public List<Integer> getScores() {
        return map.values()
                .stream()
                .map(ProductRecoMetadata::getScore)
                .collect(Collectors.toList());
    }

    public List<Integer> getShopIds() {
        return map.values()
                .stream()
                .map(ProductRecoMetadata::getShopId)
                .collect(Collectors.toList());
    }

    public List<Integer> getCategories() {
        return map.values()
                .stream()
                .map(ProductRecoMetadata::getCategoryIntId)
                .collect(Collectors.toList());
    }
}
