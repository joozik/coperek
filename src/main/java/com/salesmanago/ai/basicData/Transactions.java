package com.salesmanago.ai.basicData;

import org.apache.spark.api.java.function.FlatMapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.*;


import java.util.*;

import static com.salesmanago.ai.utils.Constants.*;
import static java.util.stream.Collectors.toList;

public class Transactions {
    public static List<String> buildTransactions(String path, SparkSession spark) {
        spark.read()
                .option("header", "false")
                .option("delimiter", "|")
                .csv(path + TRANSACTIONS)
                .flatMap((FlatMapFunction<Row, Row>) row -> {
                            final String productExtIds = row.getString(1);
                            List<String> products = new ArrayList<>();
                            if (Objects.nonNull(productExtIds)) {
                                products.addAll(Arrays.asList(productExtIds.split(",")));
                            }
                            return products.stream()
                                    .map(product -> RowFactory.create(row.getString(0),
                                            product,
                                            row.getString(2)))
                                    .collect(toList())
                                    .iterator();
                        },
                        RowEncoder.apply(new StructType()
                                .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                                .add(PRODUCT_EXT_ID_COLUMN, DataTypes.StringType)
                                .add(TRANSACTION_DATE_COLUMN, DataTypes.StringType)))
                .coalesce(1)
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(TRANSACTION_TABLE);
        return Collections.singletonList(TRANSACTION_TABLE);
    }
}
