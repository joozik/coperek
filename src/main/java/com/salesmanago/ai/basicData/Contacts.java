package com.salesmanago.ai.basicData;

import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import java.util.Collections;
import java.util.List;

import static com.salesmanago.ai.utils.Constants.*;

public class Contacts {

    public static List<String> buildContacts(String path, SparkSession spark) {
        spark.read()
                .option("header", "false")
                .option("delimiter", "|")
                .csv(path + CONTACTS)
                .map((MapFunction<Row, Row>) row ->
                                RowFactory.create(row.getString(0),
                                        row.getString(1),
                                        row.getString(2),
                                        row.getString(3)),
                        RowEncoder.apply(new StructType()
                                .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                                .add(EMAIL_COLUMN, DataTypes.StringType)
                                .add(NAME_COLUMN, DataTypes.StringType)
                                .add(MAIN_OWNER_ID_COLUMN, DataTypes.StringType)))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(CONTACT_TABLE);
        return Collections.singletonList(CONTACT_TABLE);
    }
}
