package com.salesmanago.ai.basicData;

import com.salesmanago.ai.utils.SparkSqlHelper;
import org.apache.spark.api.java.function.FilterFunction;
import org.apache.spark.api.java.function.MapFunction;
import org.apache.spark.sql.*;
import org.apache.spark.sql.catalyst.encoders.RowEncoder;
import org.apache.spark.sql.types.DataTypes;
import org.apache.spark.sql.types.StructType;

import static com.salesmanago.ai.utils.Constants.*;
import static com.salesmanago.ai.utils.Constants.AVERAGE_DAILY_VISIT_LIMIT_FOR_SANE_PERSON;
import static org.apache.spark.sql.functions.*;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

public class Visits {
    public static List<String> filterOutAndBuildVisits(String path, Integer months, SparkSession spark) {
        spark.read()
                .option("header", "false")
                .option("delimiter", "|")
                .csv(path + VISITS_FILE_NAMES_REGEXP)
                .map((MapFunction<Row, Row>) row -> {
                    String url = row.getString(4);
                    String parameters = "";
                    if (Objects.nonNull(url)) {
                        if (url.contains("?")) {
                            parameters = url.substring(url.lastIndexOf("?") + 1);
                            url = url.substring(0, url.lastIndexOf("?"));
                        }
                        if (url.contains("%3F")) {
                            parameters = url.substring(url.lastIndexOf("%3F") + 3);
                            url = url.substring(0, url.lastIndexOf("%3F"));
                        }
                    }
                    return RowFactory.create(row.getString(0),
                            row.getString(1),
                            row.getString(2),
                            Objects.nonNull(row.getString(3)) ? row.getString(3).concat(url) : "",
                            parameters);
                }, RowEncoder.apply(new StructType()
                        .add(CONTACT_ID_COLUMN, DataTypes.StringType)
                        .add(UU_ID_INT_ID_COLUMN, DataTypes.StringType)
                        .add(DATE_COLUMN, DataTypes.StringType)
                        .add(URL_COLUMN, DataTypes.StringType)
                        .add(URL_PARAMETERS_COLUMN, DataTypes.StringType)))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(VISIT_TABLE);

        findOutBots(spark, months, UU_ID_INT_ID_COLUMN)
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(ANONYMOUS_BOT_CANDIDATES_TABLE);

        findOutBots(spark, months, CONTACT_ID_COLUMN)
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(CONTACT_BOT_CANDIDATES_TABLE);

        spark.sql(SparkSqlHelper.replaceParams("SELECT * FROM %(visitTable) AS v "
                        + "LEFT ANTI JOIN %(anonymousBotTable) AS a ON v.%(uuIdIntId) = a.%(uuIdIntId) "
                        + "LEFT ANTI JOIN %(contactBotTable) AS c ON v.%(contactId) = c.%(contactId)",
                new HashMap<String, String>() {
                    {
                        put("visitTable", VISIT_TABLE);
                        put("anonymousBotTable", ANONYMOUS_BOT_CANDIDATES_TABLE);
                        put("contactBotTable", CONTACT_BOT_CANDIDATES_TABLE);
                        put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                        put("contactId", CONTACT_ID_COLUMN);
                    }
                }))
                .write()
                .format("parquet")
                .mode(SaveMode.Append)
                .saveAsTable(VISIT_FILTERED_TABLE);

        return Arrays.asList(VISIT_TABLE,
                ANONYMOUS_BOT_CANDIDATES_TABLE,
                CONTACT_BOT_CANDIDATES_TABLE,
                VISIT_FILTERED_TABLE);
    }

    public static Dataset<Row> findOutBots(SparkSession spark, Integer months, String contactInfo) {
        return spark.sql(SparkSqlHelper.replaceParams("SELECT %(contactId), %(uuIdIntId), %(date) "
                        + "FROM %(visitTable) %(whereClause)",
                new HashMap<String, String>() {
                    {
                        put("contactId", CONTACT_ID_COLUMN);
                        put("uuIdIntId", UU_ID_INT_ID_COLUMN);
                        put("date", DATE_COLUMN);
                        put("visitTable", VISIT_TABLE);
                        put("whereClause", SparkSqlHelper.recoGroupingWhereClause(contactInfo));
                    }
                }))
                .groupBy(contactInfo)
                .agg(collect_list(DATE_COLUMN).as("dates"))
                .filter((FilterFunction<Row>) row ->
                        isUserWebScrapper(row.getList(1), months))
                .drop("dates");
    }

    public static boolean isUserWebScrapper(List<String> dates, Integer months) {
        return dates.size() > months * DAYS_INT_MONTH * AVERAGE_DAILY_VISIT_LIMIT_FOR_SANE_PERSON
                || maxDailyVisitsCount(dates) > DAILY_VISIT_LIMIT_FOR_SANE_PERSON;
    }

    private static Integer maxDailyVisitsCount(List<String> dates) {
        return dates.stream()
                .reduce(new HashMap<String, Integer>(),
                        (acc, element) -> {
                            acc.compute(element.substring(0, 10), (key, val)
                                    -> (val == null)
                                    ? 1
                                    : val + 1);
                            return acc;
                        },
                        (p1, p2) -> {
                            p2.entrySet().forEach(p -> {
                                p1.compute(p.getKey(), (key, val)
                                        -> (val == null)
                                        ? p.getValue()
                                        : val + p.getValue());
                            });
                            return p1;
                        })
                .values()
                .stream()
                .mapToInt(v -> v)
                .max()
                .orElse(0);
    }

}
