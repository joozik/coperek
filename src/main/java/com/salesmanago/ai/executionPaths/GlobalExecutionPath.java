package com.salesmanago.ai.executionPaths;

import com.salesmanago.ai.actions.ExtractAction;
import com.salesmanago.ai.actions.JavaALSExample;
import com.salesmanago.ai.basicData.Contacts;
import com.salesmanago.ai.basicData.Products;
import com.salesmanago.ai.basicData.Transactions;
import com.salesmanago.ai.basicData.Visits;
import com.salesmanago.ai.transformations.*;
import com.salesmanago.ai.utils.FileUtils;
import org.apache.spark.sql.SparkSession;

import java.util.HashMap;

import static com.salesmanago.ai.utils.Constants.*;
import static com.salesmanago.ai.utils.Constants.UU_ID_INT_ID_COLUMN;

public class GlobalExecutionPath {
    private SparkSession spark;
    private String path;
    private HashMap<String, Boolean> tables = new HashMap<>();
    private Integer months;

    public GlobalExecutionPath(SparkSession spark,
                               String path) {
        this.spark = spark;
        this.path = path;
        this.months = FileUtils.coutMonthFiles(path + "/data/");
        FileUtils.removeDirectoryIfExist(path + PRODUCT_VISITS);
        FileUtils.removeDirectoryIfExist(path + "extracted-visits");
        FileUtils.removeDirectoryIfExist(path + "mahout-data");
        FileUtils.removeDirectoryIfExist(path + "report-product");
        FileUtils.removeDirectoryIfExist(path + "report-contact");
        FileUtils.removeDirectoryIfExist(path + "reco_vt");
        FileUtils.removeDirectoryIfExist(path + "myCollaborativeFilter");
        FileUtils.removeDirectoryIfExist(path + "checkpoints");
    }

    public HashMap<String, Boolean> firstStep_prepareAndFormData() {
        HashMap<String, Boolean> toRemove = new HashMap<>();
        Visits.filterOutAndBuildVisits(path, months, spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        Contacts.buildContacts(path, spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        Products.buildProducts(path, spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        Transactions.buildTransactions(path, spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ProductsExtended.buildProductVisitsFromVisits(spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        TransactionsExtended.buildProductVisitsFromTransactions(spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ExtractAction.createBasicVisitTransactionTable(spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ExtractAction.formerExtracted(path, spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ContactModel.buildGlobalContactModel(spark)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        return tables;
    }


    public HashMap<String, Boolean> secondStep_createRecoSuggestedProductsVisitedTogether() {
        HashMap<String, Boolean> toRemove = new HashMap<>();
        ReportsReco.generateSuggestedProductStructure(spark, "vt", CONTACT_ID_COLUMN)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.generateSuggestedProductStructure(spark, "vt", UU_ID_INT_ID_COLUMN)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.groupByProductAndAggregate(spark)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.buildRecoSuggestedProductTable(spark, RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsReco.buildRecoContactTable(spark, RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE, RECO_VISITED_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticRecommendation(spark, RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryRecommendation(spark, RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryStatRecommendation(spark, RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE, path)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));

        JavaALSExample.sparkRawRecommendations(spark, path);
        JavaALSExample.joinWithProductGroupByContact(spark);
        return toRemove;
    }

    public HashMap<String, Boolean> thirdStep_createRecoSuggestedProductsBoughtAfterVisit() {
        HashMap<String, Boolean> toRemove = new HashMap<>();
        ReportsReco.generateSuggestedProductStructure(spark, "bv", CONTACT_ID_COLUMN)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.groupByProductAndAggregate(spark)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.buildRecoSuggestedProductTable(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_AFTER_VISIT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsReco.buildRecoContactTable(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_AFTER_VISIT_TABLE, RECO_BOUGHT_AFTER_VISIT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticRecommendation(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_AFTER_VISIT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryRecommendation(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_AFTER_VISIT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryStatRecommendation(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_AFTER_VISIT_TABLE, path)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        return toRemove;

    }

    public HashMap<String, Boolean> fourthStep_createRecoSuggestedProductsMixedWeight() {
        HashMap<String, Boolean> toRemove = new HashMap<>();
        ReportsReco.generateSuggestedProductStructure(spark, "mw", CONTACT_ID_COLUMN)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.generateSuggestedProductStructure(spark, "mw", UU_ID_INT_ID_COLUMN)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.groupByProductAndAggregate(spark)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.buildRecoSuggestedProductTable(spark, RECO_SUGGESTED_PRODUCT_MIXED_WEIGHT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsReco.buildRecoContactTable(spark, RECO_SUGGESTED_PRODUCT_MIXED_WEIGHT_TABLE, RECO_MIXED_WEIGHT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticRecommendation(spark, RECO_SUGGESTED_PRODUCT_MIXED_WEIGHT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryRecommendation(spark, RECO_SUGGESTED_PRODUCT_MIXED_WEIGHT_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryStatRecommendation(spark, RECO_SUGGESTED_PRODUCT_MIXED_WEIGHT_TABLE, path)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        return toRemove;
    }

    public HashMap<String, Boolean> fifthStep_createRecoSuggestedProductsBoughtTogether() {
        HashMap<String, Boolean> toRemove = new HashMap<>();
        ReportsReco.generateSuggestedProductStructure(spark, "bt", CONTACT_ID_COLUMN)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.groupByProductAndAggregate(spark)
                .forEach(tableName -> {
                    this.tables.compute(tableName, (key, val) -> true);
                    toRemove.compute(tableName, (key, val) -> true);
                });
        ReportsReco.buildRecoSuggestedProductTable(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsReco.buildRecoContactTable(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_TOGETHER_TABLE, RECO_BOUGHT_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticRecommendation(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryRecommendation(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_TOGETHER_TABLE)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        ReportsStatistics.statisticCategoryStatRecommendation(spark, RECO_SUGGESTED_PRODUCT_BOUGHT_TOGETHER_TABLE, path)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        return toRemove;
    }

    public HashMap<String, Boolean> sixthStep_generateSingleFileStatistics() {
        ReportsStatistics.generateSingleFileStatistics(spark, path)
                .forEach(tableName -> this.tables.compute(tableName, (key, val) -> true));
        return tables;
    }

    public SparkSession getSpark() {
        return spark;
    }

    public String getPath() {
        return path;
    }

    public HashMap<String, Boolean> getTables() {
        return tables;
    }

    public void setTables(HashMap<String, Boolean> tables) {
        this.tables = tables;
    }
}
