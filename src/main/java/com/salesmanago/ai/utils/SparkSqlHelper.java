package com.salesmanago.ai.utils;

import java.util.HashMap;
import java.util.Map;

import static com.salesmanago.ai.utils.Constants.*;

public class SparkSqlHelper {
    public static String replaceParams(String template, Map<String, String> hashMap) {
        return hashMap.entrySet().stream().reduce(template, (s, e) -> s.replace("%(" + e.getKey() + ")", e.getValue()),
                (s, s2) -> s);
    }

    public static String recoGroupingWhereClause(String contactInfo) {
        return UU_ID_INT_ID_COLUMN.equals(contactInfo)
                ? "WHERE " + CONTACT_ID_COLUMN + " IS NULL"
                : "WHERE " + CONTACT_ID_COLUMN + " IS NOT NULL";
    }

    public static String recoGroupingWhereClause(String contactInfo, String scoringType) {
        return replaceParams("WHERE %(contactId) %(isNull) AND %(visitType) = '%(visits)'",
                new HashMap<String, String>(){
                    {
                        put("contactId", CONTACT_ID_COLUMN);
                        put("isNull", UU_ID_INT_ID_COLUMN.equals(contactInfo) ? "IS NULL" : "IS NOT NULL");
                        put("visitType", VISIT_TYPE_COLUMN);
                        put("visits", "vt".equals(scoringType) ? VIEW_VISIT_TYPE : PURCHASE_VISIT_TYPE);
                    }
                });
    }
}
