package com.salesmanago.ai.utils;


import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.*;
import java.util.AbstractMap.SimpleImmutableEntry;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.mapping;

public class UriParser {

    public static Map splitQuery(String uri) {
        if (Objects.nonNull(uri) && !uri.trim().isEmpty()) {
            try {
                final String url = uri.contains("%3D")
                        ? URLDecoder.decode(uri, "UTF-8")
                        : uri;
                return Objects.nonNull(url)
                        ? Pattern.compile("&")
                        .splitAsStream(url)
                        .map(s -> Arrays.copyOf(s.split("="), 2))
                        .filter(o -> Objects.nonNull(o[0]))
                        .map(o -> new SimpleImmutableEntry<String, String>(decode(o[0]), decode(o[1])))
                        .collect(groupingBy(SimpleImmutableEntry::getKey,
                                LinkedHashMap<String, List<String>>::new,
                                mapping(SimpleImmutableEntry::getValue, Collectors.toList())))
                        : new LinkedHashMap();
            } catch (Exception e) {
                return new LinkedHashMap();
            }
        }
        return new LinkedHashMap();
    }

    private static String decode(final String encoded) {
        try {
            return encoded == null ? null : URLDecoder.decode(encoded, "UTF-8");
        } catch(final UnsupportedEncodingException e) {
            throw new RuntimeException("Impossible: UTF-8 is a required encoding", e);
        }
    }

}
