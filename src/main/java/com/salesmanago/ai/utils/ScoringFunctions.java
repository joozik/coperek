package com.salesmanago.ai.utils;

import org.apache.commons.lang3.tuple.Pair;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.RowFactory;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.salesmanago.ai.utils.Constants.PURCHASE_VISIT_TYPE;
import static com.salesmanago.ai.utils.Constants.VIEW_VISIT_TYPE;
import static java.util.stream.Collectors.*;

public class ScoringFunctions implements Serializable {

    public static Integer scoreMixedWeight(Pair<Row, Row> rowPair) {
        Integer score = scoreBoughtAfterVisit(rowPair);
        if (Objects.nonNull(score)) {
            return score + 5;
        } else {
            score = scoreBoughtTogether(rowPair);
            if (Objects.nonNull(score)) {
                return score + 10;
            }
            return scoreVisitedTogether(rowPair);
        }
    }

    public static Integer scoreBoughtAfterVisit(Pair<Row, Row> rowPair) {
        if (isVisitAndPurchase(rowPair)) {
            LocalDateTime visitTime;
            LocalDateTime purchaseTime;
            if (PURCHASE_VISIT_TYPE.equals(rowPair.getLeft().getString(2))) {
                purchaseTime = ScoringFunctions.parseDateTime.apply(rowPair.getLeft().getString(1));
                visitTime = ScoringFunctions.parseDateTime.apply(rowPair.getRight().getString(1));
            } else {
                visitTime = ScoringFunctions.parseDateTime.apply(rowPair.getLeft().getString(1));
                purchaseTime = ScoringFunctions.parseDateTime.apply(rowPair.getRight().getString(1));
            }
            if (visitTime.isBefore(purchaseTime)) {
                long diff = ChronoUnit.DAYS.between(visitTime, purchaseTime);
                if (diff <= 3)
                    return 4;
                if (diff <= 7)
                    return 3;
                if (diff <= 31)
                    return 2;
            }
            return 1;
        } else {
            return null;
        }
    }

    private static boolean isVisitAndPurchase(Pair<Row, Row> rowPair) {
        return (PURCHASE_VISIT_TYPE.equals(rowPair.getLeft().getString(2))
                && VIEW_VISIT_TYPE.equals(rowPair.getRight().getString(2)))
                || (VIEW_VISIT_TYPE.equals(rowPair.getLeft().getString(2))
                && PURCHASE_VISIT_TYPE.equals(rowPair.getRight().getString(2)));
    }

    private static boolean isPurchaseOnly(Pair<Row, Row> rowPair) {
        return PURCHASE_VISIT_TYPE.equals(rowPair.getLeft().getString(2))
                && PURCHASE_VISIT_TYPE.equals(rowPair.getRight().getString(2));
    }

    public static Integer scoreVisitedTogether(Pair<Row, Row> rowPair) {
        long diff = ChronoUnit.HOURS.between(ScoringFunctions.parseDateTime.apply(rowPair.getLeft().getString(1)),
                ScoringFunctions.parseDateTime.apply(rowPair.getRight().getString(1)));
        if (diff <= 48)
            return 4;
        if (diff <= 3 * 24)
            return 3;
        if (diff <= 7 * 24)
            return 2;
        return 1;
    }

    public static Integer scoreBoughtTogether(Pair<Row, Row> rowPair) {
        if (isPurchaseOnly(rowPair)) {
            long diff = ChronoUnit.DAYS.between(ScoringFunctions.parseDateTime.apply(rowPair.getLeft().getString(1)),
                    ScoringFunctions.parseDateTime.apply(rowPair.getRight().getString(1)));
            if (diff <= 14)
                return 4;
            if (diff <= 31)
                return 3;
            if (diff <= 365)
                return 2;
            return 1;
        } else {
            return null;
        }
    }

    private static Function<String, LocalDateTime> parseDateTime = (str) -> {
        String dtStr = ScoringFunctions.fixTime.apply(str);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dtStr.length() == 18 ? "yyyy-MM-dd H:mm:ss" : "yyyy-MM-dd HH:mm:ss");
        return LocalDateTime.parse(dtStr, formatter);
    };

    private static Function<String, String> fixTime = (str) -> {
        int endIndex = str.lastIndexOf(".");
        return endIndex > 0 ? str.substring(0, endIndex) : str;
    };

    public static List<Row> reduceProducShopScoreToListOfRows(List<Row> list, String contactId, Integer limit) {
        return list.stream().map(r ->
                new AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>(Pair.of(r.getInt(0),
                        r.getInt(1)),
                        r.getInt(2)))
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>::getKey,
                        LinkedHashMap<Pair<Integer, Integer>, Integer>::new,
                        Collectors.summingInt(AbstractMap.SimpleImmutableEntry::getValue)), pairIntegerLinkedHashMap ->
                        pairIntegerLinkedHashMap.entrySet().stream()
                                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                                .limit(limit)
                                .map(entry -> RowFactory.create(contactId, entry.getKey().getLeft(), entry.getValue(), entry.getKey().getRight()))
                                .collect(toList())));
    }

    // TODO: przetestować
    public static Row reduceProductShopScoreToSingleRow(List<Row> list, String contactId, Long limit) {
        return list.stream().map(r ->
                new AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>(Pair.of(r.getInt(0),
                        r.getInt(1)),
                        r.getInt(2)))
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(AbstractMap.SimpleImmutableEntry<Pair<Integer, Integer>, Integer>::getKey,
                        LinkedHashMap<Pair<Integer, Integer>, Integer>::new,
                        Collectors.summingInt(AbstractMap.SimpleImmutableEntry::getValue)),
                        pairIntegerLinkedHashMap -> {
                            LinkedHashMap<Pair<Integer, Integer>, Integer> map = pairIntegerLinkedHashMap.entrySet().stream()
                                    .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                                    .limit(limit)
                                    .collect(toMap(Map.Entry::getKey, Map.Entry::getValue,
                                            Integer::sum,
                                            LinkedHashMap::new));
                            return RowFactory.create(contactId,
                                    map.keySet().stream().map(Pair::getLeft).collect(toList()).toArray(),
                                    map.values().toArray(),
                                    map.keySet().stream().map(Pair::getRight).collect(toList()).toArray());
                        }));
    }

}
