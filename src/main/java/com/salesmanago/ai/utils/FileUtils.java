package com.salesmanago.ai.utils;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.stream.Stream;

import static org.apache.commons.io.FileUtils.*;

public class FileUtils {
    public static void removeDirectoryIfExist(String path) {
        try {
            deleteDirectory(new File(path));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void createDirectory(String path) {
        File file = new File(path);
        file.mkdirs();
    }

    public static void removeUnzippedFiles(String path) {
        try(Stream<String> stream = Files.lines(Paths.get(path))) {
            stream.forEach(visitsFile -> {
                File file = new File(visitsFile);
                if (file.exists()) {
                    file.delete();
                }
            });
        } catch (IOException e){
            e.printStackTrace();
        }
    }

    public static Integer coutMonthFiles(String path) {
        return Objects.requireNonNull(new File(path).list(new FilenameFilter() {
            @Override
            public boolean accept(File dir, String filename) {
                return filename.endsWith(".csv.gz");
            }
        })).length;
    }
}
