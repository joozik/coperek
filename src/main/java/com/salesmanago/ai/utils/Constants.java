package com.salesmanago.ai.utils;

import java.util.HashMap;

public class Constants {
    // FILENAMES and relative file locations
    public static final String VISITS_LISTING_FILE_NAME = "visits.txt";
    public static final String PRODUCT_VISITS = "product_visits";
    public static final String PRODUCTS_FILE_NAME = "data/products-kopia.csv";
    public static final String VISITS_FILE_NAMES_REGEXP = "data/visit.export2.*.csv.gz";
    public static final String TRANSACTIONS = "data/transactions.csv";
    public static final String CONTACTS = "data/contacts.csv";
    public static final String HIVE_TABLE_DIR = "hive-table-dir";

    // STRING constants
    public static final String PURCHASE_VISIT_TYPE = "purchase";
    public static final String VIEW_VISIT_TYPE = "view";

    // LIMITS and other numerical constants
    public static final Long RECO_SUGGESTED_PRODUCT_LIMIT = 200L;
    public static final Long DAYS_INT_MONTH = 31L;
    public static final Long DAILY_VISIT_LIMIT_FOR_SANE_PERSON = 1000L;
    public static final Long AVERAGE_DAILY_VISIT_LIMIT_FOR_SANE_PERSON = 250L;
    public static final Long PARQUET_BLOCK_SIZE = (long) (32 * 1024 * 1024);

    // HIVE table names
    public static final String CONTACT_TABLE = "contact";
    public static final String PRODUCT_TABLE = "product";
    public static final String VISIT_TABLE = "visit";
    public static final String VISIT_FILTERED_TABLE = "visits_filtered";
    public static final String ANONYMOUS_BOT_CANDIDATES_TABLE = "anonymous_bot";
    public static final String CONTACT_BOT_CANDIDATES_TABLE = "contact_bot";
    public static final String TRANSACTION_TABLE = "transaction";
    public static final String PRODUCT_VISIT_TABLE = "product_visit";
    public static final String SUGGESTED_PRODUCTS_TO_REDUCE_TABLE = "suggested_products_to_reduce";
    public static final String SUGGESTED_PRODUCTS_NOT_GROUPED_TABLE = "suggested_products_not_grouped_vt";
    public static final String VISITS_AND_TRANSACTIONS_TABLE = "visit_transaction";
    public static final String RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE = "reco_suggested_products_visited_together";
    public static final String RECO_SUGGESTED_PRODUCT_BOUGHT_AFTER_VISIT_TABLE = "reco_suggested_products_bought_after_visit";
    public static final String RECO_SUGGESTED_PRODUCT_MIXED_WEIGHT_TABLE = "reco_suggested_products_mixed_weight";
    public static final String RECO_SUGGESTED_PRODUCT_BOUGHT_TOGETHER_TABLE = "reco_suggested_products_bought_together";
    public static final String RECO_VISITED_TOGETHER_TABLE = "reco_visited_together";
    public static final String RECO_BOUGHT_AFTER_VISIT_TABLE = "reco_bought_after_visit";
    public static final String RECO_MIXED_WEIGHT_TABLE = "reco_mixed_weight_visit";
    public static final String RECO_BOUGHT_TOGETHER_TABLE = "reco_bought_together";
    public static final String EXTRACTED_VISITS_TABLE = "extracted_visits";
    public static final String MAHOUT_DATA_TABLE = "mahout_data";
    public static final String GLOBAL_CONTACT_MODEL_TABLE = "global_contact_model";
    public static final String SPARK_ALS_RAW_TABLE = "spark_als_raw";
    public static final String SPARK_ALS_TABLE = "spark_als";
    public static final String PRODUCT_CATEGORY_STAT_TABLE = "product_category_stat_helper";
    public static final String STATISTIC_CATEGORY_RECOMMENDATION_TABLE = "statistic_category_recommendation_table";
    public static final String STATISTIC_CATEGORY_STAT_RECOMMENDATION_TABLE = "statistic_category_stat_recommendation_table";
    public static final String STATISTIC_RECOMMENDATION_TABLE = "statistic_recommendation_table";
    public static final String STATISTIC_CATEGORY_RECOMMENDATION = "statistic_category_recommendation";
    public static final String STATISTIC_CATEGORY_STAT_RECOMMENDATION = "statistic_category_stat_recommendation";
    public static final String STATISTIC_RECOMMENDATION = "statistic_recommendation";


    //COLUMN NAMES
    public static final String TRANSACTION_DATE_COLUMN = "transactionDate";
    public static final String CONTACT_ID_COLUMN = "contactId";
    public static final String VISIT_TYPE_COLUMN = "visitType";
    public static final String PRODUCT_ID_COLUMN = "productId";
    public static final String PRODUCT_UNIQUE_ID_COLUMN = "productUniqueId";
    public static final String SCORE_COLUMN = "score";
    public static final String PRODUCT_EXT_ID_COLUMN = "productExtId";
    public static final String CATEGORY_COLUMN = "category";
    public static final String SUBCATEGORY_COLUMN = "subCategory";
    public static final String CATEGORY_INT_ID_COLUMN = "categoryIntId";
    public static final String SHOP_ID_COLUMN = "shopId";
    public static final String URL_COLUMN = "url";
    public static final String URL_PARAMETERS_COLUMN = "urlParameters";
    public static final String UU_ID_INT_ID_COLUMN =  "uuIdIntId";
    public static final String DATE_COLUMN = "date";
    public static final String EMAIL_COLUMN = "email";
    public static final String NAME_COLUMN = "name";
    public static final String MAIN_OWNER_ID_COLUMN = "mainOwnerId";
    public static final String CREATED_ON_COLUMN = "createdOn";
    public static final String COMBINED_DATA_COLUMN = "combinedData";


    // MAP WITH TABLES THAT EXIST
    public static HashMap<String, Boolean> TABLES = new HashMap<String, Boolean>() {
        {
            put(CONTACT_TABLE, false);
            put(PRODUCT_TABLE, false);
            put(VISIT_TABLE, false);
            put(VISIT_FILTERED_TABLE, false);
            put(ANONYMOUS_BOT_CANDIDATES_TABLE, false);
            put(CONTACT_BOT_CANDIDATES_TABLE, false);
            put(TRANSACTION_TABLE, false);
            put(PRODUCT_VISIT_TABLE, false);
            put(SUGGESTED_PRODUCTS_TO_REDUCE_TABLE, false);
            put(SUGGESTED_PRODUCTS_NOT_GROUPED_TABLE, false);
            put(VISITS_AND_TRANSACTIONS_TABLE, false);
            put(RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE, false);
            put(EXTRACTED_VISITS_TABLE, false);
            put(MAHOUT_DATA_TABLE, false);
            put(GLOBAL_CONTACT_MODEL_TABLE, false);
        }
    };

    public static String getType(String recommendationType) {
        switch (recommendationType) {
            case RECO_SUGGESTED_PRODUCT_BOUGHT_AFTER_VISIT_TABLE:
                return "a";
            case RECO_SUGGESTED_PRODUCT_VISITED_TOGETHER_TABLE:
                return "v";
            case RECO_SUGGESTED_PRODUCT_BOUGHT_TOGETHER_TABLE:
                return "b";
            case RECO_SUGGESTED_PRODUCT_MIXED_WEIGHT_TABLE:
                return "w";
            default:
                return "c";
        }
    }
}
